/**
 * Mouse class, containing the object to calculate positions by.
 */
function Mouse() {
	console.log("Static class");
}

Mouse.getRelativePosition = function(e) {
	var canvas = document.getElementById("gameCanvas");
	var mouseX;
	var mouseY;
	
    if (e.pageX || e.pageY) {
        mouseX = e.pageX;
        mouseY = e.pageY;
    }
    else {
        mouseX = e.clientX + document.body.scrollLeft +
            document.documentElement.scrollLeft;
        mouseY = e.clientY + document.body.scrollTop +
            document.documentElement.scrollTop;
    }

    // Convert to coordinates relative to the canvas
    mouseX -= canvas.offsetLeft;
    mouseY -= canvas.offsetTop;
    
	return new Point(mouseX, mouseY);
}