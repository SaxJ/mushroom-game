/**
* @author Saxon Jensen
**/

function Vector(dx, dy) {
	this.dx = dx;
	this.dy = dy;
	this.magnitude = Math.sqrt(dx * dx + dy * dy);
}

Vector.prototype.getUnitVector = function() {
	var v = new Vector(this.dx, this.dy);
	if (this.magnitude > 0) {
		v.dx /= this.magnitude;
		v.dy /= this.magnitude;
	}
	return v;
};

Vector.prototype.dotProduct = function(v) {
	return v.dx * this.dx + v.dy * this.dy;
};

Vector.prototype.rotate = function(degrees) {
    var currentAngle = Math.atan2(this.dy, this.dx) * 180 / Math.PI;
    this.dx = Math.round(this.magnitude * Math.cos((currentAngle + degrees) * Math.PI / 180));
    this.dy = Math.round(this.magnitude * Math.sin((currentAngle + degrees) * Math.PI / 180));
}