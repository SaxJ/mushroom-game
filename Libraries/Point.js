/**
 * A javascript class representing a point in cartesian space
 * Author: Saxon Jensen
 */

function Point(x, y) {
	this.x = x;
	this.y = y;
}

Point.prototype.distanceTo = function(x, y) {
	var dx = this.x - x;
	var dy = this.y - y;
	
	return Math.sqrt(dx * dx + dy * dy);
};
