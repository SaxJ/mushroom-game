/**
 * @author Saxon Jensen
 */

/**
 * @author Saxon Jensen
 */
var conn;

var status;
var response;

if (window.MozWebSocket) {
	window.WebSocket = window.MozWebSocket;
}

function openConnection(url) {
	response = document.getElementById("response");
	status = document.getElementById("status")
	
	conn = new WebSocket(url);
	
	status.innerHTML = "trying to connect...";
	
	conn.onopen = function(e) {
		status.innerHTML = "Connected";
		conn.send('{"type":"handshake","name":"' + document.getElementById("name").value + '"}');
		document.body.setAttribute("class","connected");
	};
	
	conn.onmessage = function (e) {
		response.innerHTML = e.data + "\n" + response.innerHTML;
		var msg = e.data;
		if (msg.indexOf('{"type":"move"') != -1) {
			var m = msg.match(/.*?-(\d+)|(\d+)/);
			sendCardMove(m[1], m[2]);
		}
	};
	
	conn.onclose = function (e) {
		status.innerHTML = "Closed";
		document.body.setAttribute("class","");
	};
	
	conn.onerror = function (e) {
		console.log("Cannot Connect: " + e);
	}
}


function sendChat(chat) {
	
	conn.send('{"type":"chat", "message":"' + chat + '"}');
}

function sendCardMove(x, y) {
	conn.send('{"type":"move", "message":"' + x + '|' + y + '"}');
}
