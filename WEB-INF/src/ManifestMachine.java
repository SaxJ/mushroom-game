import java.io.File;
import java.util.ArrayList;


public class ManifestMachine {
	
	public static void main(String[] args) {
		if (args.length < 1)
			System.exit(0);
	
			File dir = new File(args[0]);
			
			if (dir.isFile()) {
				System.out.println("The directory was bad, and you should feel bad.");
				return;
			}
			
			ArrayList<String> paths = new ArrayList<String>();
			paths.add("manifest: [");
			scanDirectory(dir, paths);
			paths.add("]");
			for (String s : paths)
				System.out.println(s);
	}
	
	private static void scanDirectory(File dir, ArrayList<String> paths) {
		File[] children = dir.listFiles();
		
		for (File file : children) {
			if (file.isFile())
				paths.add("\t{src: '" + file.getPath()+ "', id: '" + file.getName() + "'},");
			else
				scanDirectory(file, paths);
		}
	}
}
