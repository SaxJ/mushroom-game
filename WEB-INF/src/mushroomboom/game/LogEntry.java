package mushroomboom.game;
/**
 * Represents a log entry for a game log.
 * 
 * @author Ben Ritter
 */
public class LogEntry {
	public enum Category {
		GAME, CHAT;
	}
	
	private Category	category;
	private String	 message;
	
	public LogEntry(Category category, String message) {
		this.category = category;
		this.message = message;
	}
	
	public String toString() {
		return category.toString() + ": " + message;
	}
}
