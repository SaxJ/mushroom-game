package mushroomboom.game;
/**
 * A card.
 * 
 * @author Ben Ritter
 */
public class Card {
	
	public enum Type {
		ONEUP(4), MAGIC(6), TOADSTOOL(8), DEATHCAP(10), PUFFBALL(12), OYSTER(14), SHIITAKE(16), BUTTON(
		        18), FIELD(20), BRIDALVEIL(22), LIONSMANE(24);
		
		private final int	number;
		
		private Type(int number) {
			this.number = number;
		}
		
		
		public int getNumber() {
			return number;
		}
		
		public int getCoinValue(int count) {
			switch (number) {
			case 0:
				return 0;
			case 4:
				if (count < 2) return 0;
				if (count == 2) return 2;
				if (count == 3) return 3;
				return 4;
			case 6:
				if (count < 2) return 0;
				if (count == 2) return 2;
				return 3;
			case 8:
				if (count < 2) return 0;
				if (count < 3) return 1;
				if (count < 4) return 2;
				if (count < 5) return 3;
				return 4;
			case 10:
				if (count < 2) return 0;
				if (count < 4) return 1;
				if (count < 5) return 2;
				if (count < 6) return 3;
				return 4;
			case 12:
				if (count < 2) return 0;
				if (count < 4) return 1;
				if (count < 6) return 2;
				if (count < 7) return 3;
				return 4;
			case 14:
				if (count < 3) return 0;
				if (count < 5) return 1;
				if (count < 6) return 2;
				if (count < 7) return 3;
				return 4;
			case 16:
				if (count < 3) return 0;
				if (count < 5) return 1;
				if (count < 7) return 2;
				if (count < 8) return 3;
				return 4;
			case 18:
				if (count < 3) return 0;
				if (count < 6) return 1;
				if (count < 8) return 2;
				if (count < 9) return 3;
				return 4;
			case 20:
				if (count < 4) return 0;
				if (count < 6) return 1;
				if (count < 8) return 2;
				if (count < 10) return 3;
				return 4;
			case 22:
				if (count < 4) return 0;
				if (count < 7) return 1;
				if (count < 9) return 2;
				if (count < 11) return 3;
				return 4;
			case 24:
				if (count < 4) return 0;
				if (count < 7) return 1;
				if (count < 10) return 2;
				if (count < 12) return 3;
				return 4;
			default:
				System.err.println("Wow, I've f**ked up big time. Maybe in the card type code.");
				return 0;
			}
		}
	}
	
	
	private Type	type;
	private boolean	offered;
	boolean	        tradable;
	
	public Card(Type type) {
		this.type = type;
	}
	
	public boolean isOffered() {
		return offered;
	}
	
	public void setOffered(boolean offered) {
		this.offered = offered;
	}
	
	public Type getType() {
		return type;
	}
	
}
