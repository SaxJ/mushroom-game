package mushroomboom.game;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.Vector;

import mushroomboom.events.Draw;
import mushroomboom.events.Harvest;
import mushroomboom.events.Plant;
import mushroomboom.events.Trade;
import mushroomboom.exceptions.EndGame;
import mushroomboom.exceptions.InvalidMessage;
import mushroomboom.exceptions.IllegalMove;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


/**
 * A game implementation that implements the original rules of the game.
 * 
 * The cards in play are 6-20 inclusive, 3rd fields are allowed for 3 coins (TODO) and the game ends
 * after two shuffles.
 * 
 * @author Ben Ritter
 */
public class OriginalGame extends Game {
	// Constants
	public static final int	PLAYER_MAX_FIELDS	= 3;
	public static final int	PLAYER_START_FIELDS	= 2;
	public static final int	EXTRA_FIELD_COST	= 3;
	
	public static final int	HAND_START_SIZE	    = 5;
	public static final int	SHUFFLES	        = 2;
	
	public static final int	DRAW_SEEDS_HAND	    = 3;
	public static final int	DRAW_SEEDS_TABLE	= 2;
	
	private enum Turn {
		MUST_PLANT, CAN_PLANT, MUST_DRAW, TRADING, WAITING;
	};
	
	private int	             shufflesToGo;
	
	private Player	         turnPlayer;
	private Turn	         turnStage;
	
	private LinkedList<Card>	stock;
	private Stack<Card>	     discard;
	
	
	public OriginalGame(WebSocketServlet server, String id, long randomSeed) {
		super(server, id, randomSeed);
	}
	
	
	protected void startGame() {
		gameState = State.INGAME;
		stock = new LinkedList<Card>();
		discard = new Stack<Card>();
		
		// Tell everyone the game has started, and set up the order.
		JSONArray order = new JSONArray();
		Player lastPlayer = players.get(players.size() - 1);
		for (Player p : players) {
			order.add(p.getId());
			lastPlayer.setNextPlayer(p);
			lastPlayer = p;
		}
		broadcast(Message.startGame(order));
		
		tracker.startGame();
		
		// Setting up the stock and discard
		shufflesToGo = SHUFFLES;
		for (Card.Type c : Card.Type.values()) {
			// This is for the "original" gametype
			if (6 <= c.getNumber() && c.getNumber() <= 20) {
				for (int i = 0; i < c.getNumber(); i++) {
					discard.push(new Card(c));
				}
			}
		}
		shuffle();
		

		// Deal out the cards
		Message move = new Message("moveCard");
		move.put("source", "STOCK");
		move.put("destination", "HAND");
		for (int i = 0; i < HAND_START_SIZE; i++) {
			for (Player p : players) {
				Card card = stock.poll();
				p.hand.add(card);
				move.put("destinationPlayer", p.getId());
				move.put("cardType", card.getType().getNumber());
				p.send(move);
				move.put("cardType", 0);
				broadcast(move, p);
			}
		}
		
		// Whose turn it is
		turnPlayer = players.get(0);
		turnStage = Turn.MUST_PLANT;
		broadcast(Message.turn(turnPlayer, turnStage.toString()));
	}
	
	
	/**
     * Ends the game, determines the winner and let's everyone know.
     */
    protected void endGame() {
    	// Calculate everyone's position
    	int found = 0;
    	int lastMax = 99999;
    	for (int pos = 1; pos <= players.size(); pos += found) {
    		int max = 0;
    		for (Player p : players) {
    			if (p.coins < lastMax)
    				max = Math.max(max, p.coins);
    		}
    		lastMax = max;
    		
    		found = 0;
    		for (Player p : players) {
    			if (p.coins == max) {
    				p.finishingPlace = pos;
    				found++;
    			}
    		}
    	}
    	
    	// Send this information
    	List<Map<String, Object>> ps = new ArrayList<Map<String, Object>>();
    	for (Player p : players) {
    		Map<String, Object> m = new HashMap<String, Object>();
    		m.put("player", p.getId());
    		m.put("coins", p.coins);
    		m.put("place", p.finishingPlace);
    		ps.add(m);
    	}
    	
    	JSONObject m = new Message("gameOver");
    	m.put("players", ps);
    	broadcast(m);
    	
    	gameState = State.FINISHED;
    }


	/**
	 * Handles a message that is about the game.
	 * 
	 * @throws InvalidMessage iff the message is found to be invalid
	 */
	protected void handleMessage(Player player, JSONObject message) throws InvalidMessage {
		String type = (String) message.get("type");
		
		try {
			if (gameState != Game.State.INGAME)
			    throw new InvalidMessage("We are not playing right now.");
			
			// Move Card
			if (type.equals("moveCard")) {
				tryMoveCard(player, message);
			}

			// Harvest
			else if (type.equals("harvest")) {
				int fieldIndex = Message.getInt(message, "field");
				if (fieldIndex > player.fields.size() || fieldIndex < 0)
				    throw new InvalidMessage("field is out of bounds.");
				tryHarvestField(player, fieldIndex);
			}

			// Trade Request
			else if (type.equals("tradeRequest")) {
				
				if ((Boolean) message.get("trade")) {
					Player other = getPlayer((String) message.get("otherPlayer"));
					if (other == null) throw new InvalidMessage("otherPlayer not found.");
					
					tryTradeRequest(player, other);
				}

				else {
					if (player.tradePartner == null)
					    throw new InvalidMessage("You are not trading with anyone.");
					
					cancelTrade(player);
				}
			}


			// Offering a card
			else if (type.equals("offerCard")) {
				tryOfferCard(player, message);
			}

			else {
				throw new InvalidMessage("Unknown message type: " + type);
			}
			
		} catch (IllegalMove e) {
			player.send(Message.illegalMove(e.getMessage()));
		} catch (EndGame e) {
			endGame();
		}
	}
	
	/**
	 * Offers a card if it's within the game rules.
	 * 
	 * @param player the player
	 * @param message the message
	 * @throws InvalidMessage if the message is found to be invalid
	 * @throws IllegalMove if the move is illegal
	 */
	private void tryOfferCard(Player player, JSONObject message) throws InvalidMessage, IllegalMove {
		List<Card> location;
		if (message.get("location").equals("HAND")) location = player.hand;
		else if (message.get("location").equals("TABLE")) location = player.table;
		else throw new IllegalMove("You cannot offer that card.");
		
		Card c = location.get(Message.getInt(message, "index"));
		if (!c.tradable) throw new IllegalMove("That card cannot be traded again.");
		if (c.isOffered() == (Boolean) message.get("offer"))
		    throw new InvalidMessage("That card is already in that offered state.");
		
		// Cancel any trades that are going on with this player
		if (player.tradePartner != null) {
			player.send(Message.tradeRequest(false, player, player.tradePartner));
			player.tradePartner.send(Message.tradeRequest(false, player, null));
			player.tradePartner = null;
		}
		for (Player o : players) {
			if (o.tradePartner == player) {
				player.send(Message.tradeRequest(false, o, null));
				o.send(Message.tradeRequest(false, o, player));
				o.tradePartner = null;
			}
		}
		
		// Offer the card and tell everyone
		c.setOffered((Boolean) message.get("offer"));
		
		message.put("player", player.getId());
		message.put("cardType", c.getType().getNumber());
		if (c.isOffered()) broadcast(message);
		else {
			player.send(message);
			if (message.get("location").equals("HAND")) message.put("cardType", 0);
			broadcast(message, player);
		}
	}
	
	
	/**
	 * Tries to move a card based on a moveCard message.
	 * 
	 * @param player TODO
	 * @param message the message from the client
	 * @return true iff the move was made
	 * @throws EndGame if the game should end
	 * @throws InvalidMessage if the message is found to be invalid
	 * @throws IllegalMove if the move is invalid
	 */
	private void tryMoveCard(Player player, JSONObject message) throws EndGame, InvalidMessage,
	        IllegalMove {
		// Planting from TABLE
		if (message.get("source").equals("TABLE")) {
			if (!message.get("destination").equals("FIELD"))
			    throw new IllegalMove("You can not do that with this card.");
			
			int fieldIndex = Message.getInt(message, "destinationIndex");
			int tableIndex = Message.getInt(message, "sourceIndex");
			if (fieldIndex < 0 || fieldIndex >= player.fields.size())
			    throw new InvalidMessage("fieldIndex is out of bounds.");
			if (tableIndex < 0 || tableIndex >= player.table.size())
			    throw new InvalidMessage("tableIndex is out of bounds.");
			
			Card card = player.table.get(tableIndex);
			
			tryAddCardToField(player, card, fieldIndex);
			player.table.remove(card);
			
			message.put("destinationPlayer", player.getId());
			message.put("sourcePlayer", player.getId());
			message.put("cardType", card.getType().getNumber());
			broadcast(message);
			
			tracker.track(new Plant(player, card.getType()));
			
			card.setOffered(false);
			
			tryFinishTurn();
			
			return;
		}
		
		// It has to be your turn to do the rest of the moves
		if (turnPlayer != player) throw new IllegalMove("It's not your turn.");
		
		// Drawing...
		if (message.get("source").equals("STOCK")) {
			// ...to TABLE
			if (turnStage == Turn.MUST_DRAW || turnStage == Turn.CAN_PLANT) {
				message.put("destination", "TABLE");
				Card.Type[] cards = new Card.Type[DRAW_SEEDS_TABLE];
				for (int i = 0; i < DRAW_SEEDS_TABLE; i++) {
					Card card = drawFromStock();
					player.table.add(card);
					cards[i] = card.getType();
					
					message.put("destinationPlayer", player.getId());
					message.put("cardType", card.getType().getNumber());
					broadcast(message);
					
					card.setOffered(false);
				}
				tracker.track(new Draw(player, cards));
				
				turnStage = Turn.TRADING;
				broadcast(Message.turn(turnPlayer, turnStage.toString()));
				
				return;
			}
			
			// ...to HAND (ending turn)
			if (turnStage == Turn.TRADING && player.table.size() == 0) {
				message.put("destination", "HAND");
				Card.Type[] cards = new Card.Type[DRAW_SEEDS_HAND];
				for (int i = 0; i < DRAW_SEEDS_HAND; i++) {
					Card card = drawFromStock();
					player.hand.add(card);
					cards[i] = card.getType();
					
					message.put("destinationPlayer", player.getId());
					message.put("cardType", 0);
					broadcast(message, player);
					message.put("cardType", card.getType().getNumber());
					player.send(message);
					
					card.setOffered(false);
				}
				tracker.track(new Draw(player, cards));
				
				turnStage = Turn.WAITING;
				if (!tryFinishTurn()) {
					// if we haven't finished the turn, tell everyone we're waiting.
					broadcast(Message.turn(turnPlayer, turnStage.toString()));
				}
				
				return;
			}
			
			throw new IllegalMove("You cannot draw cards now.");
		}
		
		// Planting from HAND
		if (message.get("source").equals("HAND")) {
			if (!message.get("destination").equals("FIELD"))
			    throw new IllegalMove("You cannot move that card there");
			
			if (turnStage == Turn.MUST_DRAW)
			    throw new IllegalMove("You can only plant up to two cards at the start of your turn.");
			if (turnStage != Turn.MUST_PLANT && turnStage != Turn.CAN_PLANT)
			    throw new IllegalMove("You can only plant cards at the start of your turn.");
			
			int sourceIndex = Message.getInt(message, "sourceIndex");
			if (sourceIndex < 0 || sourceIndex >= player.hand.size())
			    throw new InvalidMessage("sourceIndex is out of bounds");
			if (sourceIndex != 0)
			    throw new IllegalMove("You must plant the card at the start of your hand.");
			
			int fieldIndex = Message.getInt(message, "destinationIndex");
			

			Card card = player.hand.get(0);
			
			tryAddCardToField(player, card, fieldIndex);
			player.hand.remove(card);
			
			message.put("destinationPlayer", player.getId());
			message.put("sourcePlayer", player.getId());
			message.put("cardType", card.getType().getNumber());
			broadcast(message);
			
			tracker.track(new Plant(player, card.getType()));
			
			if (turnStage == Turn.MUST_PLANT) turnStage = Turn.CAN_PLANT;
			else turnStage = Turn.MUST_DRAW;
			broadcast(Message.turn(turnPlayer, turnStage.toString()));
			
			card.setOffered(false);
			return;
		}
		
		throw new InvalidMessage("I can't figure out what you mean with that moveCard message.");
	}
	
	/**
	 * Harvests a player's field if it's within the game rules.
	 * 
	 * @param player the player
	 * @param fieldIndex the field index <b>which is not checked for validity</b>
	 * @throws IllegalMove if the field cannot be harvested
	 */
	private void tryHarvestField(Player player, int fieldIndex) throws IllegalMove {
		if (player.fields.get(fieldIndex).size() == 1) {
			for (int i = 0; i < player.fields.size(); i++) {
				if (player.fields.get(i).size() > 1) {
					throw new IllegalMove(
					        "You cannot sell that field. You must sell your fields with more than one card first.");
				}
			}
		}
		
		Vector<Card> field = player.fields.get(fieldIndex);

		tracker.track(new Harvest(player, field.get(0).getType(), field.size()));

		int val = field.get(0).getType().getCoinValue(field.size());
		player.coins += val;
		
		for (int i = 0; i < val; i++) field.remove(0);
		discard.addAll(field);
		field.clear();
		
		broadcast(Message.harvest(player, fieldIndex, val));
	}
	
	
	/**
	 * Sets a player to be trading if it's within the game rules. Does the trade if needed.
	 * 
	 * @param player the player offering
	 * @param other the player they're offering to
	 * @throws IllegalMove if the move is found to be illegal
	 */
	private void tryTradeRequest(Player player, Player other) throws IllegalMove {
		if (turnStage != Turn.TRADING)
		    throw new IllegalMove("You can only trade during the trading stage of someone's turn.");
		if (turnPlayer != player && turnPlayer != other)
		    throw new IllegalMove("You can only trade with the player whose turn it is.");
		

		if (player.tradePartner == other)
		    throw new IllegalMove("You are already trading with that player!");
		
		if (player.tradePartner != null) {
			cancelTrade(player);
		}
		

		if (other.tradePartner == player) { // Do the trade
			broadcast(Message.doTrade(player, other));
			tracker.track(new Trade(other,  other.tradeOfferedCards(player),
					                player, player.tradeOfferedCards(other)));
			tracker.track(new Trade(player, player.tradeOfferedCards(other),
									other,  other.tradeOfferedCards(player))); 
		} else {
			player.send(Message.tradeRequest(true, player, other));
			other.send(Message.tradeRequest(true, player, null));
			player.tradePartner = other;
		}
	}
	
	
	private void cancelTrade(Player player) {
		player.send(Message.tradeRequest(false, player, player.tradePartner));
		player.tradePartner.send(Message.tradeRequest(false, player, null));
		
		player.tradePartner = null;
	}
	
	
	/**
	 * Adds a card to a player's field if it can be within the the game rules.
	 * 
	 * @param player the player
	 * @param card the card
	 * @param fieldIndex the index of the field
	 * @throws IllegalMove if the card cannot be added
	 */
	private void tryAddCardToField(Player player, Card card, int fieldIndex) throws IllegalMove {
		Vector<Card> field = player.fields.get(fieldIndex);
		if (field.size() > 0 && field.get(0).getType() != card.getType())
		    throw new IllegalMove("You cannot plant a card of this type in that field.");
		
		field.add(card);
	}
	
	
	private Card drawFromStock() throws EndGame {
		if (stock.size() == 0) tryShuffle();
		return stock.pop();
	}
	
	/**
	 * Shuffles the cards if there are shuffles to go, ends the game otherwise.
	 * 
	 * @throws EndGame if the game should end
	 */
	private void tryShuffle() throws EndGame {
		if (shufflesToGo-- == 0) {
			throw new EndGame();
		} else {
			shuffle();
		}
	}
	
	private void shuffle() {
		while (!discard.isEmpty()) {
			Card c = discard.remove(nextRandomInt(discard.size()));
			c.tradable = true;
			stock.add(c);
		}
		broadcast(Message.shuffle(shufflesToGo, stock.size()));
	}
	
	private boolean tryFinishTurn() {
		if (turnStage != Turn.WAITING) return false;
		
		for (Player p : players) {
			if (p.table.size() != 0) return false;
		}
		
		turnPlayer = turnPlayer.nextPlayer();
		if (turnPlayer.hand.size() == 0)
			turnStage = Turn.MUST_DRAW;
		else
			turnStage = Turn.MUST_PLANT;
		broadcast(Message.turn(turnPlayer, turnStage.toString()));
		
		// Unoffer all cards
		for (Player p: players) {
			Message m = new Message("offerCard");
			m.put("player", p.getId());
			m.put("offered", false);
			
			m.put("location", "HAND");
			int i = 0;
			for (Card c: p.hand){
				if (c.isOffered()) {
					c.setOffered(false);
					m.put("index", i);
					m.put("cardType", c.getType().getNumber());
					p.send(m);
					m.put("cardType", 0);
					broadcast(m, p);
				}
				i++;
			}
			
		}
		
		return true;
	}
	

}
