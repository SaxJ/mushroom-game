package mushroomboom.game;

import java.util.HashMap;
import java.util.Random;
import java.util.Vector;

import mushroomboom.exceptions.InvalidMessage;
import mushroomboom.game.LogEntry.Category;
import mushroomboom.stats.GameTracker;

import org.json.simple.JSONObject;



/**
 * A single game. A list of {@link Player} objects is associated with this game, and the game state
 * is stored between this object and the Player objects. Game rules are implemented in subclasses.
 * 
 * @author Ben Ritter
 */
public abstract class Game {
	
	public enum State {
		LOBBY, INGAME, PAUSED, FINISHED
	};
	
	private Vector<LogEntry>	    log	= new Vector<LogEntry>();
	
	// Housekeeping
	private WebSocketServlet	    server;
	String	                        id;
	private Message	                joinGameMessage;
	private Random	                random;
	
	// Players
	public Vector<Player>           players;
	private HashMap<String, Player>	playerIds;
	

	State	                        gameState;

	protected GameTracker           tracker;
	
	
	public Game(WebSocketServlet server, String id, long randomSeed) {
		this.server = server;
		this.id = id;
		
		random = new Random(randomSeed);
		
		players = new Vector<Player>();
		playerIds = new HashMap<String, Player>();
		gameState = State.LOBBY;
		
		tracker = new GameTracker(this);
		
		joinGameMessage = new Message("joinGame");
		joinGameMessage.put("id", id);
	}
	
	public void broadcastAndLog(JSONObject message) {
		broadcastAndLog(message, null);
	}
	
	public void broadcastAndLog(JSONObject message, Player except) {
		LogEntry.Category cat = null;
		
	    log.add(new LogEntry(cat, message.toJSONString()));	
	    broadcast(message, except);
    }

	/**
	 * Sends a message to the clients of all players in the game.
	 * 
	 * @param message the message to send
	 * @param except this player will not receive the message
	 */
	public void broadcast(JSONObject message) {
		broadcast(message, null);
	}
	
	/**
	 * Sends a message to the clients of almost all of the players in the game.
	 * 
	 * @param message the message to send
	 * @param except this player will not receive the message
	 */
	public void broadcast(JSONObject message, Player except) {
		for (Player p : players) {
			if (p == except) continue;
			
			p.send(message);
		}
	}
	
	/** @return true iff a new player can be added to the game */
	public boolean acceptingNewPlayers() {
		return gameState == State.LOBBY;
	}
	
	/**
	 * Adds a new player to this game, if this game is accepting players.
	 * 
	 * @param player the player to add
	 * @return true iff the player was added
	 */
	public boolean addPlayer(Player player) {
		if (acceptingNewPlayers()) {
			player.game = this;
			player.send(joinGameMessage);
			
			// Tell the new player of all the other players...
			Message npm = new Message("playerJoined");
			for (Player p : players) {
				npm.put("player", p.getId());
				player.send(npm);
			}
			// and tell everyone else about this new player.
			npm.put("player", player.getId());
			broadcast(npm);
			
			players.add(player);
			playerIds.put(player.getId(), player);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Handles the case when a player disconnects.
	 * 
	 * During the lobby stage they are removed but in game the game is paused as we wait for someone
	 * to take their place.
	 * 
	 * @param player the player that disconnected.
	 */
	public boolean pausedAfterDisconnected(Player player) {
		if (gameState == State.LOBBY || gameState == State.FINISHED) {
			removePlayer(player);
			return false;
		} else {
			if (gameState != State.PAUSED) {
				gameState = State.PAUSED;
				broadcast(Message.gameState(gameState));
			}
			
			broadcast(Message.connectionDropped(player));
			
			boolean stillGood = false;
			for (Player p : players)
				if (p.isConnected()) stillGood = true;
			if (!stillGood) {
				server.removeGame(this);
				return false;
			} else {
				return true;
			}
		}
	}
	
	/**
	 * Handles the case where a player that was disconnected in the middle of a game reconnects.
	 * 
	 * @param player the player that reconnected
	 */
	public void playerReconnected(Player player) {
		broadcast(Message.connectionBack(player), player);
		player.send(new Message("continueGame"));
		
		for (Player p : players) {
			if (!p.isConnected()) return;
		}
		
		gameState = State.INGAME;
		broadcast(Message.gameState(gameState));
	}
	
	/**
	 * Ends the game because a player left.
	 * 
	 * @param p the player who left
	 */
	public void playerLeft(Player p) {
		removePlayer(p);
		endGame();
	}

	/**
	 * Removes the player and sends a playerLeft message the everyone else.
	 */
	private void removePlayer(Player player) {
		players.remove(player);
		playerIds.remove(player.getId());
		
		if (players.size() == 0) {
			server.removeGame(this);
		} else {
			broadcast(Message.playerLeft(player));
		}
	}
	
	/**
	 * Checks if everyone is ready and starts the game if that is the case.
	 * 
	 * @param player the player who has become ready
	 */
	public void playerWantsToStartGame() {
		if (gameState != State.LOBBY) return;
		
		for (Player p : players) {
			if (!p.isReady()) return;
		}
		
		startGame();
	}
	
	/** Sets up and starts the game. */
	abstract protected void startGame();
	abstract protected void endGame();
	
	/**
	 * Tries to move a card based on a message.
	 * 
	 * @param player the player who sent this message
	 * @param message the message
	 * @throws InvalidMessage iff the message was found to be invalid
	 */
	abstract protected void handleMessage(Player player, JSONObject message) throws InvalidMessage;
	
	
	// Helper Methods
	/** @return the player that the id identifies. */
	public Player getPlayer(String id) {
		return playerIds.get(id);
	}
	
	protected int nextRandomInt(int limit) {
		return random.nextInt(limit);
	}
	
	
	protected void log(LogEntry.Category category, String message) {
		log.add(new LogEntry(category, message));
	}
	
	public String getLogString() {
		StringBuffer sb = new StringBuffer();
		for (LogEntry e : log) {
			sb.append(e.toString()).append("\n");
		}
		return sb.toString();
	}
	
}
