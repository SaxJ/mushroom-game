package mushroomboom.game;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import mushroomboom.exceptions.InvalidMessage;
import mushroomboom.stats.Tracker;
import mushroomboom.stats.tracker.CoinCollectorTracker;
import mushroomboom.stats.tracker.DebugAchievementTracker;
import mushroomboom.stats.tracker.DrawTracker;
import mushroomboom.stats.tracker.HarvestAchievementTracker;
import mushroomboom.stats.tracker.MedalTracker;
import mushroomboom.stats.tracker.TradingTracker;

import org.json.simple.JSONObject;


public class Player {
	// Housekeeping
	String	                 name;
	PlayerConnection	     connection;
	
	// Stats
	public List<Tracker>     trackers;
	public Set<String>       achievements; // temp
	
	// Game
	public Game	             game;
	private WebSocketServlet server;
	
	private boolean	         ready;
	
	// In Game
	private Player	         nextPlayer;
	
	int	                     coins;
	
	/** The player's hand. 0 is the frontmost, new cards are added to the back. */
	Vector<Card>	         hand;
	/** The cards on the players table. */
	Vector<Card>	         table;
	/** The player's fields. */
	Vector<Vector<Card>>	 fields;
	
	Player	                 tradePartner;
	public int	             finishingPlace;
	
	
	public Player(WebSocketServlet server, PlayerConnection connection, String name) {
		this.server = server;
		this.name = name;
		this.connection = connection;
		
		// Stats
		trackers = new Vector<Tracker>();
		trackers.add(new CoinCollectorTracker(this));
		trackers.add(new DebugAchievementTracker(this));
		trackers.add(new DrawTracker(this));
		trackers.add(new HarvestAchievementTracker(this));
		trackers.add(new MedalTracker(this));
		trackers.add(new TradingTracker(this));
		achievements = new HashSet<String>();
		
		ready = false;
		
		reset();
	}
	
	public void reset() {
		coins = 0;
		hand = new Vector<Card>();
		table = new Vector<Card>();
		
		fields = new Vector<Vector<Card>>();
		for (int i = 0; i < 2; i++) {
			fields.add(new Vector<Card>());
		}
		
		tradePartner = null;
		
		ready = false; // probably not needed, but we reset sometimes.
	}
	
	/**
	 * @return true if this player is ready to start the game.
	 */
	public boolean isReady() {
		return ready;
	}
	
	public String getId() {
		return name;
	}
	
	
	public void setNextPlayer(Player p) {
		nextPlayer = p;
	}
	
	public Player nextPlayer() {
		return nextPlayer;
	}
	
	/** @return the number of coins the player has */
	public int getCoins() {
		return coins;
	}
	
	
	/**
	 * Sends a message to the client for this player.
	 * 
	 * @param message the message to send.
	 */
	public void send(JSONObject message) {
		if (connection != null) connection.send(message);
	}
	
	/**
	 * Parses a message sent from this players client and acts on it.
	 * 
	 * @param message the message received
	 * @throws InvalidMessage iff the message is found to be invalid
	 */
	public void dealWithMessage(JSONObject message) throws InvalidMessage {
		String type = (String) message.get("type");
		if (type == null) throw new InvalidMessage("Message type missing!");
		
		if (type.equals("chat") || type.equals("typing")) {
			if (game == null) throw new InvalidMessage("You are not connected to a game.");
			
			message.put("player", getId());
			game.broadcast(message);
			
			return;
		}
		
		if (type.equals("newGame")) {
			if (game != null) throw new InvalidMessage("You are already in a game.");
			
			if (server.createGame(this, (String) message.get("id"))) return;
			else throw new InvalidMessage("Game name invalid or already taken.");
			// TODO this is not really an invalid message
		}
		
		if (type.equals("joinGame")) {
			if (game != null) throw new InvalidMessage("You are already in a game.");
			
			if (server.playerJoinGame(this, (String) message.get("id"))) return;
			else throw new InvalidMessage("That game is not longer around.");
			// TODO this is not really an invalid message
		}
		
		if (type.equals("ready")) {
			boolean old = ready;
			ready = (Boolean) message.get("ready");
			if (ready == old) return;
			
			game.broadcast(Message.ready(this.getId(), ready));
			
			return;
		}
		
		if (type.equals("startGame")) {
			game.playerWantsToStartGame();
			
			return;
		}
		

		// If we've made it this far, the message must be for the game to handle.
		if (game == null) throw new InvalidMessage("You are not in a game.");
		else game.handleMessage(this, message);
		
	}
	
	/**
	 * Moves all cards that are being offered to another player's table.
	 * <p>
	 * All cards that are moved are set to non-tradable and not offered. moveCard messages are sent
	 * for each card.
	 * 
	 * @param otherPlayer the player to send the cards to.
	 */
	Card.Type[] tradeOfferedCards(Player otherPlayer) {
		List<Card.Type> cards = new Vector<Card.Type>();
		Message m = new Message("moveCard");
		m.put("sourcePlayer", getId());
		m.put("destinationPlayer", otherPlayer.getId());
		m.put("destination", "TABLE");
		
		m.put("source", "HAND");
		cards.addAll(tradeOfferedCardsFrom(hand, otherPlayer, m));
		
		m.put("source", "TABLE");
		cards.addAll(tradeOfferedCardsFrom(table, otherPlayer, m));
		
		tradePartner = null;
		return cards.toArray(new Card.Type[0]);
	}
	
	/**
	 * Helper method for {@link tradeOfferedCards} that moves offered cards from a location to
	 * another player.
	 * 
	 * @param loc the location to search
	 * @param otherPlayer the player to send the cards to
	 * @param m the message to modify and send
	 * @return a list of cards that where traded
	 */
	private List<Card.Type> tradeOfferedCardsFrom(List<Card> loc, Player otherPlayer, Message m) {
		List<Card.Type> cards = new Vector<Card.Type>();
		for (int i = loc.size() - 1; i >= 0; i--) {
			if (loc.get(i).isOffered()) {
				Card card = loc.remove(i);
				cards.add(card.getType());
				
				m.put("sourceIndex", i);
				m.put("cardType", card.getType().getNumber());
				game.broadcast(m);
				
				card.setOffered(false);
				card.tradable = false;
				otherPlayer.table.add(card);
			}
		}
		return cards;
	}
	
	
	/**
	 * Reacts to the player's connection being terminated.
	 */
	public void disconnected() {
		this.connection = null;
		server.playerDisconnected(this);
	}
	
	
	public void reconnected(PlayerConnection connection) {
		this.connection = connection;
	}
	
	public boolean isConnected() {
		return connection != null;
	}

	public int getHandCount() {
	    return hand.size();
    }
	
}
