package mushroomboom.game;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import mushroomboom.exceptions.RefuseConnection;

import com.caucho.websocket.WebSocketServletRequest;


public class WebSocketServlet extends HttpServlet {
	private static final long	serialVersionUID	= 1L;
	
	Set<Player>	                lonelyPlayers, players;
	private Map<String, Game>	games;
	
	private Random	            random;
	
	private Map<String, Player>	disconnectedPlayers;
	
	public WebSocketServlet() {
		lonelyPlayers = new HashSet<Player>();
		players = new HashSet<Player>();
		disconnectedPlayers = new HashMap<String, Player>();
		games = new HashMap<String, Game>();
		
		random = new Random();
	}
	
	/**
	 * Handles new connections.
	 * 
	 * @param req the HTTP request
	 * @param response the response that will be sent
	 */
	public void service(HttpServletRequest req, HttpServletResponse res) throws IOException,
	        ServletException {
		// Create a connection and upgrade it to a websocket
		PlayerConnection listener;
		listener = new PlayerConnection(this);
		
		WebSocketServletRequest wsReq = (WebSocketServletRequest) req;
		wsReq.startWebSocket(listener);
	}
	
	/**
	 * Sends a message to the clients of all player that are not in a game.
	 * 
	 * @param message the message to send
	 */
	public void broadcast(Message message) {
		for (Player p : lonelyPlayers) {
			p.send(message);
		}
	}
	
	/**
	 * Handles a new connection from a client and does the handshake.
	 * 
	 * @param connection the new connection
	 * @param id the clients player id
	 * @return the newly created player
	 * @throws RefuseConnection if the connection was refused
	 */
	public Player newConnection(PlayerConnection connection, JSONObject message) throws RefuseConnection {
		String id = (String) message.get("id");
		if (id == null || id.length() == 0 || !id.trim().equals(id))
			throw new RefuseConnection("Invalid name");
		for (Player p : players)
			if (p.getId().equals(id)) throw new RefuseConnection("Name taken");
		
		Player p = null;
		if (disconnectedPlayers.containsKey(id)) {
			// the player has dropped connection 
			p = disconnectedPlayers.remove(id);
			
			if (p.game.id.equals(message.get("gameId"))) {
				// revival time
				p.reconnected(connection);
				p.send(new Message("handshake"));
				
				players.add(p);
				p.game.playerReconnected(p);
			} else {
				// they have lost everything.
				p.game.playerLeft(p);
				p = null;
			}
		}
		
		if (p == null) { // normal connection
			p = new Player(this, connection, id);
			lonelyPlayers.add(p);
			players.add(p);
			p.send(new Message("handshake"));
			
			for (String gid : games.keySet()) {
				if (games.get(gid).acceptingNewPlayers()) p.send(Message.newGame(gid));
			}
		}
		
		return p;
	}
	
	/**
	 * Tries to add a player to a game. This may fail if the game doesn't exist or isn't accepting
	 * players, etc.
	 * 
	 * @param player the player to add
	 * @param id the id of the game
	 * @return true iff the player was successfully added to the game
	 */
	public boolean playerJoinGame(Player player, String id) {
		if (!lonelyPlayers.contains(player)) return false;
		if (!games.containsKey(id)) return false;
		if (!games.get(id).addPlayer(player)) return false;
		lonelyPlayers.remove(player);
		
		return true;
	}
	
	/**
	 * Handles the case when a player, that is not in a game, disconnects.
	 * 
	 * @param player the player that was disconnected
	 */
	public void playerDisconnected(Player player) {
		if (player.game == null) {
			lonelyPlayers.remove(player);
		} else if (player.game.pausedAfterDisconnected(player)) {
			disconnectedPlayers.put(player.getId(), player);
		}
		players.remove(player);
	}
	
	public boolean createGame(Player player, String id) {
		if (games.containsKey(id)) return false;
		
		games.put(id, new OriginalGame(this, id, random.nextLong()));
		playerJoinGame(player, id);
		
		broadcast(Message.newGame(id));
		
		return true;
	}
	
	public void removeGame(Game game) {
		for (Player p : players) {
			disconnectedPlayers.remove(p);
		}
		games.remove(game.id);
	}
}