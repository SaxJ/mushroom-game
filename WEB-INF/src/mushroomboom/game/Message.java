package mushroomboom.game;
import mushroomboom.exceptions.InvalidMessage;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


/**
 * A message that can be sent to the client. This is basically a slightly quicker way to create a
 * message, setting it's type in the constructor.
 * 
 * @author Ben Ritter
 * 
 */
public class Message extends JSONObject {
	
	private static final long	serialVersionUID	= 1L;
	
	public Message(String type) {
		this.put("type", type);
	}
	
	public static Message newGame(String id) {
		Message m = new Message("newGame");
		m.put("id", id);
		return m;
	}
	
	public static Message startGame(JSONArray order) {
		Message m = new Message("startGame");
		m.put("order", order);
		return m;
	}
	
	public static Message ready(String id, boolean ready) {
		Message m = new Message("ready");
		m.put("player", id);
		m.put("ready", ready);
		return m;
	}
	
	public static JSONObject error(String code, String description) {
		Message m = new Message("error");
		m.put("code", code);
		m.put("description", description);
		return m;
	}
	
	public static JSONObject gameState(Game.State state) {
		Message m = new Message("gameState");
		switch (state) {
		case PAUSED:
			m.put("state", "PAUSE");
			break;
		case INGAME:
			m.put("state", "RESUME");
			break;
		case LOBBY:
			m.put("state", "CANCEL");
			break;
		}
		return m;
	}
	
	public static JSONObject playerLeft(Player player) {
		Message m = new Message("playerLeft");
		m.put("player", player.getId());
		return m;
	}
	
	public static JSONObject turn(Player turnPlayer, String turnStage) {
		Message m = new Message("turn");
		m.put("player", turnPlayer.getId());
		m.put("stage", turnStage.toString());
		return m;
	}
	
	public static int getInt(JSONObject message, String key) throws InvalidMessage {
		Long n = (Long) message.get(key);
		if (n == null) throw new InvalidMessage("The attribute " + key + " was not set.");
		return n.intValue();
	}
	
	public static JSONObject harvest(Player player, int fieldIndex, int coins) {
		Message m = new Message("harvest");
		m.put("player", player.getId());
		m.put("field", fieldIndex);
		m.put("coins", coins);
		return m;
	}
	
	public static JSONObject doTrade(Player player, Player otherPlayer) {
		Message m = new Message("doTrade");
		m.put("player", player.getId());
		m.put("otherPlayer", otherPlayer.getId());
		return m;
	}
	
	public static JSONObject tradeRequest(boolean trade, Player player, Player otherPlayer) {
		Message m = new Message("tradeRequest");
		m.put("trade", trade);
		if (player != null) m.put("player", player.getId());
		if (otherPlayer != null) m.put("otherPlayer", otherPlayer.getId());
		return m;
	}
	
	public static JSONObject shuffle(int shufflesToGo, int cards) {
		Message m = new Message("shuffle");
		m.put("shufflesToGo", shufflesToGo);
		m.put("cards", cards);
		return m;
	}
	
	public static Message connectionDropped(Player player) {
		Message m = new Message("connectionDropped");
		m.put("player", player.getId());
		return m;
	}
	
	public static Message connectionBack(Player player) {
		Message m = new Message("connectionBack");
		m.put("player", player.getId());
		return m;
	}

	public static JSONObject illegalMove(String reason) {
		Message m = new Message("illegalMove");
		m.put("reason", reason);
		return m;
    }

	public static JSONObject achievement(Player player, String achievement) {
		Message m = new Message("achievement");
		m.put("player", player.getId());
		m.put("achievement", achievement);
		return m;
    }
}
