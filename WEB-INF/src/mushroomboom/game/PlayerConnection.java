package mushroomboom.game;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Reader;

import mushroomboom.exceptions.InvalidMessage;
import mushroomboom.exceptions.RefuseConnection;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.caucho.websocket.AbstractWebSocketListener;
import com.caucho.websocket.WebSocketContext;


public class PlayerConnection extends AbstractWebSocketListener {
	
	private static final int	VERSION	= 6;
	
	private static JSONParser	parser	= new JSONParser();
	
	private WebSocketContext	context;
	private WebSocketServlet	server;
	private Player	          player;
	
	public PlayerConnection(WebSocketServlet server) {
		this.server = server;
	}
	
	public void onClose(WebSocketContext arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}
	
	public void onDisconnect(WebSocketContext arg0) throws IOException {
		player.disconnected();
	}
	
	public void onReadBinary(WebSocketContext arg0, InputStream arg1) throws IOException {
		// TODO Auto-generated method stub
		
	}
	
	public void onReadText(WebSocketContext context, Reader in) throws IOException {
		try {
			
			JSONObject message = (JSONObject) parser.parse(in);
			
			if (player == null) {
				try {
					if (!"handshake".equals(message.get("type"))) {
						// excuse the backwards wording, but get(type) may be null
						throw new RefuseConnection("Expected Protocol Handshake");
					}
					if (Message.getInt(message, "version") != VERSION) {
						throw new RefuseConnection(
						        "Versions do not match. This server is at version " + VERSION);
					}
					
					this.player = server.newConnection(this, message);
					
				} catch (RefuseConnection e) {
					Message m = new Message("connectionRefused");
					m.put("reason", e.getMessage());
					send(m);
					context.close();
				}
				return;
			}
			
			player.dealWithMessage(message);
			

		} catch (ParseException e) {
			send(Message.error("JSONParse", e.getMessage()));
		} catch (InvalidMessage e) {
			send(Message.error("invalidMessage", e.getMessage()));
		} catch (ClassCastException e) {
			send(Message.error("invalidMessage", "Attribute of wrong type: " + e.getMessage()
			        + ". " + e.getStackTrace()[0].toString()));
		} catch (Exception e) {
			send(Message.error("serverError", ""+e+"\n" + e.getMessage() + "\n" + e.getStackTrace()[0] + "\n"
			        + e.getStackTrace()[1]));
		}
	}
	
	public void send(JSONObject message) {
		PrintWriter response;
		
		try {
			response = context.startTextMessage();
			response.print(message.toJSONString());
			response.close();
		} catch (IOException e) {
			// TODO disconnect
			e.printStackTrace();
		}
		
	}
	
	public void onStart(WebSocketContext context) throws IOException {
		this.context = context;
	}
	
	public void onTimeout(WebSocketContext arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}
	

}
