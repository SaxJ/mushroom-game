package mushroomboom.exceptions;

/**
 * This is thrown when a message is found to be invalid.
 * 
 * @author Ben Ritter
 */
public class InvalidMessage extends Exception {
    private static final long serialVersionUID = 1L;
    
    public InvalidMessage(String reason) {
	    super(reason);
    }
}
