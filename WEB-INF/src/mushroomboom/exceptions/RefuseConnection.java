package mushroomboom.exceptions;
/**
 * This is thrown when a connection is refused.
 * 
 * @author Ben Ritter
 */
public class RefuseConnection extends Exception {
    private static final long serialVersionUID = 1L;

	public RefuseConnection(String reason) {
		super(reason);
    }
}
