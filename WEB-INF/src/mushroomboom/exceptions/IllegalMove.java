package mushroomboom.exceptions;

/**
 * Represents a move that is against the game rules.
 * 
 * @author Ben Ritter
 */
public class IllegalMove extends Exception {
	private static final long serialVersionUID = 1L;
    
    public IllegalMove(String reason) {
	    super(reason);
    }
}
