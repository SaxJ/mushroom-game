package mushroomboom.exceptions;
/**
 * Signifies that the game should be ended.
 * 
 * @author Ben Ritter
 */
public class EndGame extends Exception {
    private static final long serialVersionUID = 1L;

}
