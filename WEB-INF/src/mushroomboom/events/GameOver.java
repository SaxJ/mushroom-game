package mushroomboom.events;

import mushroomboom.game.Player;

public class GameOver implements Event {
	public Player player;
	public int position;
	public int coins;
	public String medal;
	
	public GameOver(Player player, int position, int coins, String medal) {
	    this.player = player;
	    this.position = position;
	    this.coins = coins;
	    this.medal = medal;
    }
}
