package mushroomboom.events;

import mushroomboom.game.Card;
import mushroomboom.game.Player;
import mushroomboom.game.Card.Type;

public class Harvest implements Event {
	public Player player;
	public Card.Type type;
	public int count;
	
	public Harvest(Player player, Type type, int count) {
	    super();
	    this.player = player;
	    this.type = type;
	    this.count = count;
    }
}
