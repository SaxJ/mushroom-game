package mushroomboom.events;

import mushroomboom.game.Player;
import mushroomboom.game.Card;
import mushroomboom.game.Card.Type;

public class Trade implements Event {
	public Player player1;
	public Card.Type[] cards1;
	public Player player2;
	public Card.Type[] cards2;
	
	public Trade(Player player1, Type[] player1Cards, Player player2, Type[] player2Cards) {
	    this.player1 = player1;
	    this.cards1 = player1Cards;
	    this.player2 = player2;
	    this.cards2 = player2Cards;
    }
	
}
