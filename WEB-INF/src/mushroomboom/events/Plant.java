package mushroomboom.events;

import mushroomboom.game.Card;
import mushroomboom.game.Player;
import mushroomboom.game.Card.Type;

public class Plant implements Event {
	public Player player;
	public Card.Type type;
	
	public Plant(Player player, Type type) {
	    super();
	    this.player = player;
	    this.type = type;
    }
}
