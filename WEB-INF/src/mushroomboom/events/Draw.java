package mushroomboom.events;
import mushroomboom.game.Player;
import mushroomboom.game.Card;
import mushroomboom.game.Card.Type;

public class Draw implements Event {
	public Player player;
	public Card.Type[] cards;
	
	public Draw(Player player, Type[] cards) {
	    this.player = player;
	    this.cards = cards;
    }
}
