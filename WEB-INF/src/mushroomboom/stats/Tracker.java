package mushroomboom.stats;

import mushroomboom.game.Message;
import mushroomboom.game.Player;


/**
 * A Tracker stores information about some aspect of a players stats. A player will have a list of
 * them. An implementation should implement one or more of the tracker types, to receive
 * notifications of events. The tracker can then decide when it is the right time to awards
 * achievements etc.
 * 
 * When player objects are created, a bunch of different trackers are created (and data is loaded 
 * for stats that last between games). During the game, events are given to the trackers.
 */
public abstract class Tracker {
	protected Player player;
	
	public Tracker(Player player) {
		this.player = player;
	}
	
	protected void awardAchievement(String achievement) {
		
		if (player.achievements.add(achievement))
			player.game.broadcast(Message.achievement(player, achievement));
		//TODO
	}
	
	//public abstract void load();
	//public abstract void save();
	
}
