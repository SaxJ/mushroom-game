package mushroomboom.stats;

import java.util.ArrayList;
import java.util.List;

import mushroomboom.events.*;
import mushroomboom.game.Game;
import mushroomboom.game.Player;
import mushroomboom.stats.tracker.type.DrawTracker;
import mushroomboom.stats.tracker.type.GameOverTracker;
import mushroomboom.stats.tracker.type.HarvestTracker;
import mushroomboom.stats.tracker.type.PlantTracker;
import mushroomboom.stats.tracker.type.GameStartTracker;
import mushroomboom.stats.tracker.type.TradeTracker;

/**
 * The GameTracker aggregates all trackers for all players. The game sends all events through 
 * this class, and this class passes the events on to the appropriate trackers. 
 */
public class GameTracker {
	private Game game;
	
	public GameTracker(Game game) {
		this.game = game;
	}
	
	private List<DrawTracker>     dts = new ArrayList<DrawTracker>();
	private List<HarvestTracker>  hts = new ArrayList<HarvestTracker>();
	private List<PlantTracker>    pts = new ArrayList<PlantTracker>();
	private List<TradeTracker>    tts = new ArrayList<TradeTracker>();
	private List<GameOverTracker> gts = new ArrayList<GameOverTracker>();
	
	public void track(Event e) {
		if (e instanceof Draw)    for (DrawTracker t :     dts) t.track((Draw) e);
		if (e instanceof Harvest) for (HarvestTracker t :  hts) t.track((Harvest) e);
		if (e instanceof Plant)   for (PlantTracker t :    pts) t.track((Plant) e);
		if (e instanceof Trade)   for (TradeTracker t :    tts) t.track((Trade) e);
		if (e instanceof GameOver)for (GameOverTracker t : gts) t.track((GameOver) e);
	}
	
	public void startGame() {
		for (Player p: game.players) {
			for (Tracker t: p.trackers) {
				if (t instanceof DrawTracker)     dts.add((DrawTracker) t);
				if (t instanceof HarvestTracker)  hts.add((HarvestTracker) t);
				if (t instanceof PlantTracker)    pts.add((PlantTracker) t);
				if (t instanceof TradeTracker)    tts.add((TradeTracker) t);
				if (t instanceof GameOverTracker) gts.add((GameOverTracker) t);
				if (t instanceof GameStartTracker) ((GameStartTracker) t).trackStartGame();
			}
		}
	}
}
