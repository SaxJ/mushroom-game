package mushroomboom.stats.tracker;

import mushroomboom.events.Plant;
import mushroomboom.game.Card;
import mushroomboom.game.Player;
import mushroomboom.stats.Tracker;
import mushroomboom.stats.tracker.type.PlantTracker;

public class DebugAchievementTracker extends Tracker implements PlantTracker {
	
	public DebugAchievementTracker(Player player) {
	    super(player);
    }

	public void track(Plant e) {
		if (e.player != player) return;
		
		if (e.type == Card.Type.MAGIC)
			awardAchievement("Feel The Magic");
	}
	
}
