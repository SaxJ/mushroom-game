package mushroomboom.stats.tracker.type;

import mushroomboom.events.Harvest;

public interface HarvestTracker {
	public void track(Harvest e);
}
