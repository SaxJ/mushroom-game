package mushroomboom.stats.tracker.type;

import mushroomboom.events.Plant;

public interface PlantTracker {
	public void track(Plant e);
}
