package mushroomboom.stats.tracker.type;

import mushroomboom.events.Trade;

public interface TradeTracker {
	public void track(Trade e);
}
