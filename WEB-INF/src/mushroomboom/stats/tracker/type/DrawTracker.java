package mushroomboom.stats.tracker.type;

import mushroomboom.events.Draw;


public interface DrawTracker {
	public void track(Draw e);
}
