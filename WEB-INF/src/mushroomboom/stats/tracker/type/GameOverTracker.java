package mushroomboom.stats.tracker.type;

import mushroomboom.events.GameOver;

public interface GameOverTracker {
	public void track(GameOver e);
}
