package mushroomboom.stats.tracker.type;

public interface GameStartTracker {
	public void trackStartGame();
}
