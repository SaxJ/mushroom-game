package mushroomboom.stats.tracker;

import java.util.EnumMap;

import mushroomboom.events.Harvest;
import mushroomboom.game.Card;
import mushroomboom.game.Player;
import mushroomboom.stats.Tracker;
import mushroomboom.stats.tracker.type.HarvestTracker;

public class CoinCollectorTracker extends Tracker implements HarvestTracker {

	private static final int TYPE_TARGET = 5; // 100
	private static final int TOTAL_TARGET = 10; // 1000

	private EnumMap<Card.Type, Integer> coinsCollected;
	private int totalCoins = 0;
	
	public CoinCollectorTracker(Player player) {
	    super(player);
	    coinsCollected = new EnumMap<Card.Type, Integer>(Card.Type.class);
	    for (Card.Type t: Card.Type.values()) coinsCollected.put(t, 0);
    }

	public void track(Harvest e) {
		if (e.player != player) return;
		
		int c = e.type.getCoinValue(e.count);
		if (c == 0) return;
		
		if (coinsCollected.get(e.type) < TYPE_TARGET  &&  coinsCollected.get(e.type) + c >= TYPE_TARGET) {
			awardAchievement(e.type.name() + " Farmer");
		}
		if (totalCoins < TOTAL_TARGET && totalCoins + c >= TOTAL_TARGET) {
			awardAchievement("Rich");
		}
		coinsCollected.put(e.type, coinsCollected.get(e.type) + c);
		totalCoins += c;
	}
	
}
