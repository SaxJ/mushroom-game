package mushroomboom.stats.tracker;

import mushroomboom.events.GameOver;
import mushroomboom.game.Player;
import mushroomboom.stats.Tracker;
import mushroomboom.stats.tracker.type.GameOverTracker;

/**
 * A Tracker that tracks How many medals the player has won. Also tracks medal achievements.
 *  
 * @author Ben Ritter
 */
public class MedalTracker extends Tracker implements GameOverTracker {
	
	public MedalTracker(Player player) {
	    super(player);
    }

	private int goldMedals;
	private int silverMedals;
	private int bronzeMedals;
	
	public void track(GameOver e) {
		if (e.player != player) return;
		
		if (e.medal.equals("gold"))   goldMedals++;
		if (e.medal.equals("silver")) silverMedals++;
		if (e.medal.equals("bronze")) bronzeMedals++;
		
		if (goldMedals + silverMedals + bronzeMedals == 1)
			awardAchievement("Beginner");
		if (goldMedals + silverMedals + bronzeMedals == 10)
			awardAchievement("Trainee");
		if (goldMedals + silverMedals + bronzeMedals == 100)
			awardAchievement("Qualified");
		if (goldMedals + silverMedals + bronzeMedals == 1000)
			awardAchievement("Veteran");
		
	}
	
}
