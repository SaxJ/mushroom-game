package mushroomboom.stats.tracker;

import java.util.Arrays;

import mushroomboom.events.Trade;
import mushroomboom.game.Player;
import mushroomboom.stats.Tracker;
import mushroomboom.stats.tracker.type.TradeTracker;

public class TradingTracker extends Tracker implements TradeTracker {
	
	public TradingTracker(Player player) {
	    super(player);
    }

	public void track(Trade e) {
		if (e.player1 != player) return;
		
		if (e.cards1.length == e.cards2.length) {
			Arrays.sort(e.cards1);
			Arrays.sort(e.cards2);
			boolean good = true;
			for (int i = 0; i < e.cards1.length; i++) if (e.cards1[i] != e.cards2[i]) good = false;
			if (good) awardAchievement("6 of one, half a dozen of the other");
		}
		
		//TODO add tracker.endTurn() and track Generous 
	}
	
}
