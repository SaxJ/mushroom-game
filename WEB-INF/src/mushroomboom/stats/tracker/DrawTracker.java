package mushroomboom.stats.tracker;

import mushroomboom.events.Draw;
import mushroomboom.game.Player;
import mushroomboom.stats.Tracker;

public class DrawTracker extends Tracker implements mushroomboom.stats.tracker.type.DrawTracker {
	
	public DrawTracker(Player player) {
	    super(player);
    }

	public void track(Draw e) {
		if (e.player != player) return;
		
		if (player.getHandCount() >= 10) awardAchievement("Planner");
	}
	
}
