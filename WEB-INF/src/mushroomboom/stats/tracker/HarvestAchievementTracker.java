package mushroomboom.stats.tracker;

import java.util.EnumMap;

import mushroomboom.events.GameOver;
import mushroomboom.events.Harvest;
import mushroomboom.game.Card;
import mushroomboom.game.Player;
import mushroomboom.stats.Tracker;
import mushroomboom.stats.tracker.type.GameOverTracker;
import mushroomboom.stats.tracker.type.HarvestTracker;
import mushroomboom.stats.tracker.type.GameStartTracker;


/**
 * A tracker that tracks harvest related achievements.
 * 
 * <ul>
 * <li>Full Harvest</li>
 * <li>Full Season</li>
 * </ul>
 * 
 * @author Ben Ritter
 */
public class HarvestAchievementTracker extends Tracker implements HarvestTracker, GameStartTracker,
        GameOverTracker {
	
	public HarvestAchievementTracker(Player player) {
		super(player);
		fullHarvestedType = new EnumMap<Card.Type,Boolean>(Card.Type.class);
	}
	
	private boolean fullSeasonStillGood;
	private EnumMap<Card.Type,Boolean> fullHarvestedType;
	private EnumMap<Card.Type,Boolean> harvestedType;
	
	public void trackStartGame() {
		fullSeasonStillGood = true;
		harvestedType = new EnumMap<Card.Type,Boolean>(Card.Type.class);
	}
	
	public void track(Harvest e) {
		if (e.player != player) return;
		
		harvestedType.put(e.type, true);
		
		if (e.type.getCoinValue(100) == e.type.getCoinValue(e.count)) {
			awardAchievement("Full Harvest");
			
			fullHarvestedType.put(e.type, true);
			if (!fullHarvestedType.containsValue(false)) awardAchievement("Well Rounded");
		} else {
			fullSeasonStillGood = false;
		}
	}
	
	public void track(GameOver e) {
		if (e.player != player) return;
		
		if (fullSeasonStillGood) awardAchievement("Full Season");
		
		if (!harvestedType.containsValue(false)) awardAchievement("Jack of All Trades");
	}
	
}
