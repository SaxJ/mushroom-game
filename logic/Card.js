/**
 * A namespace holding information and methods about cards.
 */

Card.WIDTH  =  80;
Card.HEIGHT = 120;

// Load the card images
Card.images = {};
Card.images[0] = new Image();
Card.images[0].src  = "Images/card-0.png";
Card.images[-1] = new Image();
Card.images[-1].src = "Images/coin.png";
for (var i = 6; i <= 20; i+=2) {
	Card.images[i] = new Image();
	Card.images[i].src = "Images/card-" + i + ".png";
}

Card.names = {
	4: "1UP Mushroom",
	6: "Magic Mushroom",
	8: "Toadstool",
	10: "Death Cap Mushroom",
	12: "Puff Ball",
	14: "Oyster Mushroom",
	16: "Shiitake Mushroom",
	18: "Button Mushroom",
	20: "Field Mushroom",
	22: "Bridal Veil Mushroom",
	24: "Lions Mane Mushroom"
}

Card.offeredShadow = new Shadow("blue", 0, 0, 20);


function Card() {
	throw new Error("Card: static class");
}

/**
 * Creates a card of type 0 (unknown).
 */
Card.createCard = function() {
	var card = new Bitmap(Card.images[0]);
	card.regX = Card.WIDTH / 2; // this is the offset of the x, y from the image origin
	card.regY = Card.HEIGHT / 2;
	card.x = Game.stock.x;
	card.y = Game.stock.y;
	card.scaleX = card.scaleY = card.scale = Game.stock.scale;
	card.zIndex = 0;
	card.floating = false;
	
	card.type = 0;
	card.offered = false;
	card.location = null;
	card.locationIndex = null;

	card.animation = null;
	card.shadow = null;
	
	// wrapper function to provide scope for the event handlers:
	(function(target) {
		target.onPress = function(evt) {
		
			var offset = {
				x : target.x - evt.stageX,
				y : target.y - evt.stageY
			};
			
			if (target.animation != null) {
				Drawing.removeAnimation(target.animation);
			}
			
			// Makes the card sit on top of all others
			card.floating = true;
			Card.sortCards();
			
			// this will be active until the user releases the mouse button:
			evt.onMouseMove = function(ev) {
				if (Math.abs(target.x - ev.stageX + offset.x) > 2)
					target.moved = true;
				target.x = ev.stageX + offset.x;
				target.y = ev.stageY + offset.y;
				Drawing.update = true;
			};
			
			evt.onMouseUp = function(e) {
				target.floating = false;
				Card.sortCards();
				
				if (target.moved) Actions.handleDropCard(target);
				else if (card.type != 0) InfoBubble.showForCard(target);
				
				target.moved = false;
				
			};
		};
		
		target.onMouseOver = function() {
			target.scaleX = target.scaleY = target.scale * 1.05;
			Drawing.update = true;
		};
		
		target.onMouseOut = function() {
			target.scaleX = target.scaleY = target.scale;
			Drawing.update = true;
		};
		
		target.onDoubleClick = function() {
			InfoBubble.hide();
			var dc = this.getDoubleClick();
			if (dc != null) Connection.sendMessage(dc.message);
		};
		
		target.getPos = function() {
			return target.location.getCardPos(target);
		}
	})(card);
	
	card.getDoubleClick = function() {
		if (this.location.player != Game.primaryPlayer) return null;
		
		if (this.location.name == "FIELD") return {
			name:    "Harvest Field",
			message: {type: "harvest", field: this.location.fieldIndex}
		};
		else return {
			name:    "Offer Card",
			message: {
				type: "offerCard",
				offer: !this.offered,
				location: this.location.name,
				index: this.locationIndex
			}
		};
	}
	
	return card;
};

Card.turnIntoCoin = function (card) {
	card.location.player.addCoins(1);
	Card.destroy(card);
}

Card.destroy = function (card) {
	if (card.location != null) {
		card.location.removeCard(card);
	}
	Game.container.removeChild(card);
}

Card.sortCards = function() {
	Game.container.sortChildren(function(a, b) {
		if (a.floating &&  ! b.floating) return 1;
		if ( ! a.floating && b.floating) return -1;
		azi = a.zIndex || 0;
		bzi = b.zIndex || 0;
		return azi - bzi;
	});
}

Card.coinValue = function(type, count) {
	switch (type) {
		case 0:
			return 0;
		case 4:
			if (count < 2) return 0;
			if (count == 2) return 2;
			if (count == 3) return 3;
			return 4;
		case 6:
			if (count < 2) return 0;
			if (count == 2) return 2;
			return 3;
		case 8:
			if (count < 2) return 0;
			if (count < 3) return 1;
			if (count < 4) return 2;
			if (count < 5) return 3;
			return 4;
		case 10:
			if (count < 2) return 0;
			if (count < 4) return 1;
			if (count < 5) return 2;
			if (count < 6) return 3;
			return 4;
		case 12:
			if (count < 2) return 0;
			if (count < 4) return 1;
			if (count < 6) return 2;
			if (count < 7) return 3;
			return 4;
		case 14:
			if (count < 3) return 0;
			if (count < 5) return 1;
			if (count < 6) return 2;
			if (count < 7) return 3;
			return 4;
		case 16:
			if (count < 3) return 0;
			if (count < 5) return 1;
			if (count < 7) return 2;
			if (count < 8) return 3;
			return 4;
		case 18:
			if (count < 3) return 0;
			if (count < 6) return 1;
			if (count < 8) return 2;
			if (count < 9) return 3;
			return 4;
		case 20:
			if (count < 4) return 0;
			if (count < 6) return 1;
			if (count < 8) return 2;
			if (count < 10) return 3;
			return 4;
		case 22:
			if (count < 4) return 0;
			if (count < 7) return 1;
			if (count < 9) return 2;
			if (count < 11) return 3;
			return 4;
		case 24:
			if (count < 4) return 0;
			if (count < 7) return 1;
			if (count < 10) return 2;
			if (count < 12) return 3;
			return 4;
		default:
			throw new Rrror("invalid vals!");
	}
}