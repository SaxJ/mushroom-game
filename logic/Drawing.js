/**
 * Static drawing class, for on-canvas formatting of the UI
 **/

Drawing.LEFT_FORMAT_INDENT = 80;

Drawing.animations = new Array();
Drawing.update = false;

Drawing.TURNCOLOUR = "orange";

function Drawing() {
	throw new Error("Drawing: Static Class");
}

/**
 * This is called by the ticket every frame.
 */
Drawing.tick = function() {
	if (Drawing.animations.length > 0  ||  Drawing.update) {
		for (var i = 0; i < Drawing.animations.length; i++) {
			var a = Drawing.animations[i];
			a.tick();
		}
		if (CardAnimation.currentDelay) CardAnimation.currentDelay--;
		
		Drawing.update = false;
		Game.stage.update();
	}
};

/**
 * Adds a CardAnimation on a card.
 */
Drawing.animateCard = function(card, options) {
	Drawing.animations.push(new CardAnimation(card, options));
};

/**
 * Removes the animation from the list and from it's card.
 */
Drawing.removeAnimation = function(animation) {
	if (animation == null) return;
	
	animation.doCallback();
	
	for (var i = 0; i < Drawing.animations.length; i++) {
		if (Drawing.animations[i] == animation) {
			Drawing.animations.splice(i, 1);
			return;
		}
	}
	animation.card.animation = null;
};

/**
 * Draws the transparent shapes used to indicate different locations on the board.
 */
Drawing.drawLocations = function() {
	var colour = {FIELD:"darkGreen", HAND:"green", TABLE:"green", DISCARD:"yellow"};
	var locationsContainer = new Container();
	
	for (i in Game.locations) {
		var loc = Game.locations[i]
		
		// Make the graphic
		var g = new Graphics();
		g.beginStroke(colour[loc.name]).beginFill(colour[loc.name]);
		g.drawRect(0, 0, loc.width*loc.scale, loc.height*loc.scale);
		
		// Make and position a shape from it
		var fin = new Shape(g);
		fin.rotation = loc.rotation;
		fin.alpha = 0.4;
		fin.x = loc.x;
		fin.y = loc.y;
		
		// Add that shape to the container
		locationsContainer.addChild( fin );
	}
	
	Game.stage.addChildAt(locationsContainer,0);
	Game.stage.update();
};