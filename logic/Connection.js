/**
 * @author Saxon Jensen
 * @author Ben Ritter
 */

if (window.MozWebSocket) {
	window.WebSocket = window.MozWebSocket;
}

function Connection() {
	throw new Error("STATIC CLASS: Connection");
}

Connection.VERSION = 6;

Connection.conn = null;


Connection.openConnection = function() {
	
	PLAYERID = document.getElementById("name").value;
	
	var url = window.location.href;
	url = 'ws' + url.substring( url.indexOf(':') );
	url = url.substring( 0, url.lastIndexOf('/') ) + '/shrooms.ws';
	console.log("Connecting to " + url + "...");
	Connection.conn = new WebSocket(url);
	
	Connection.conn.onopen = function(e) {
		Connection.reconnectAttempts = 0;
		
		console.log("Connected!");
		var m = {type: "handshake", id: PLAYERID, version: Connection.VERSION };
		if (Game.id) m.gameId = Game.id;
		Connection.sendMessage(m);
		
		showAll(   connectedList);
		hideAll(notConnectedList);
	};

	Connection.conn.onmessage = function (e) {
		console.log("GOT  " + e.data);
		loadingIndicator.src = "Images/loading.png";
		var message = JSON.parse(e.data);
		
		switch(message.type) {
			case "chat":
				chatBox(Game.playersById[message.player].htmlName() + ": " + parseChatMessage(message.message), "chat");
				Game.playersById[message.player].isNotTyping();
				break;
			
			case "achievement":
				chatBox(Game.playersById[message.player].htmlName() + ' has earned <span class="achievementName">' + message.achievement + '</span>', "achievement");
			
			case "typing":
				Game.playersById[message.player].isTyping();
				break;
			
			case "handshake":
				chatBox("Connected to a working "+GAMENAME+" server v" + Connection.VERSION + " :)", "server");
				break;
			
			case "connectionRefused":
				chatBox("The connection was refused: " + message.reason, "server");
				Connection.conn.close();
				break;
			
			case "newGame": // a new game is available
				chatBox('Game available: <span class="pseudoLink" onclick="joinGame(\''+message.id+'\')">'+message.id+'</span>', "server");
				
				// DEBUG
				if (autoJoinName) {
					joinGame(message.id);
				}
				break;
			
			case "joinGame":
				Game.primaryPlayer = new Player(PLAYERID, PLAYERID);
				Game.playersById[PLAYERID] = Game.primaryPlayer;
				Game.state = "LOBBY";
				Game.id = message.id;
				
				chatBox("Joined game: " + message.id, "game");
				showAll(   inGameList);
				hideAll(notInGameList);
				
				// DEBUG
				if (autoJoinName) {
					document.getElementById("lobbyReady").click();
				}
				break;
			
			case "playerJoined":
				Game.playersById[message.player] = new Player(message.player, message.player);
				chatBox(Game.playersById[message.player].htmlName() + " joined the game", "player");
				break;
			case "playerLeft":
				chatBox(Game.playersById[message.player].htmlName() + " left the game", "player");
				Game.playersById[message.player] = undefined;
				break;
			
			case "connectionDropped":
				chatBox(Game.playersById[message.player].htmlName() + " disconnected", "player");
				break;
			case "connectionBack":
				chatBox(Game.playersById[message.player].htmlName() + " is back!", "player");
				break;
			
			case "gameState":
				chatBox("New game state: " + message.state, "server");
				Game.state = (message.state == "RESUME" ? "INGAME" : "PAUSED");
				break;
				
			case "ready":
				var a = Game.playersById[message.player].htmlName() + " is ";
				if (message.player == PLAYERID) {
					a = "You are ";
					document.getElementById("lobbyReady")    .style.display = (message.ready ? "none" : "block");
					document.getElementById("lobbyNotReady") .style.display = (message.ready ? "block" : "none");
					document.getElementById("lobbyStartGame").style.display = (message.ready ? "block" : "none");
				}
				chatBox(a + (message.ready ? "" : "not ") + " ready", "player");
				break;
			
			case "error":
				chatBox("ERROR (from server): " + message.code + " (" + message.description + ")", "error");
				break;
				
			default:
				Game.interpretMessage(message);
		}
	};

	Connection.conn.onclose = function (e) {
		chatBox("Disconnected :(", "server");
		switch (Game.state) {
			case "INGAME":
			case "PAUSED":
				// DEBUG
				if (autoJoinName && Connection.reconnectAttempts > 0) {
					setTimeout("window.location.reload()", 5000);
					return;
				}
				
				if (Connection.reconnectAttempts < 3) setTimeout("Connection.openConnection()", Connection.reconnectAttempts++ * 2000);
				else leaveGame();
				break;
				
			case "LOBBY":
			case "FINISHED":
				leaveGame();
				break;
				
			default:
				//not even connected to a game, maybe not at all
		}
		showAll(notConnectedList);
		hideAll(   connectedList);
	};

	Connection.conn.onerror = function (e) {
		console.log("WebSocket Error: " + e);
	};
};

Connection.sendMessage = function(message) {
	loadingIndicator.src = "Images/loading.gif";
	if (typeof(message) != "string") {
		message = JSON.stringify(message);
	}
	Connection.conn.send(message);
	console.log("SENT " + message);
};
