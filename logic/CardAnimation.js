/**
 * @author Saxon Jensen
 * @author Ben Ritter
 **/

CardAnimation.DELAY_TIME      = 7; //delay between two cards animating
CardAnimation.MAX_LENGTH      = 50;
CardAnimation.MIN_LENGTH      = 10;
CardAnimation.MIN_FLIP_LENGTH = 30;


CardAnimation.currentDelay = 0;

/**
 * An animation that animates a card to the cardLocation it is supposed to be at.
 *
 * @param card the card to animate
 * @param options (optional) a dictionary of options 
 *       {delay:    the delay between this card starting to animate and the next card starting to animate,
 *        callback: a function to call (with the card as an argument) when the animation ends }
 */
function CardAnimation(card, options) {
	if (card == undefined) return;
	options = options || {};
	
	Drawing.removeAnimation(card.animation);
	card.animation = this;
	this.card = card;
	this.dest = card.getPos();
	this.callback = options.callback || null;
	this.updates = -CardAnimation.currentDelay; // negative this.updates means we haven't started yet
	if (options.delay === undefined) CardAnimation.currentDelay += CardAnimation.DELAY_TIME;
	else                             CardAnimation.currentDelay += options.delay;

	var vector = new Vector(this.dest.dx - card.x, this.dest.dy - card.y);
	this.length = vector.magnitude / 30;
	if (this.length < CardAnimation.MIN_LENGTH) this.length = CardAnimation.MIN_LENGTH;
	if (this.length > CardAnimation.MAX_LENGTH) this.length = CardAnimation.MAX_LENGTH;
	
	// Draw the card above all others
	card.floating = true;
	Card.sortCards();
	
	// Determine the flipping
	this.xScaleFactor = 1; 
	this.dXScaleFactor = 0;
	if (card.image != Card.images[card.type]) {
		if (this.length < CardAnimation.MIN_FLIP_LENGTH) this.length = CardAnimation.MIN_FLIP_LENGTH;
		this.dXScaleFactor = -2 / this.length;
	}
	
	
	// Determine the movement
	this.unitMove = vector.getUnitVector(); // a unit vector in the direction that we should move
	this.velocity = vector.magnitude / this.length;

	// Determine the rotation
	//TODO get the correct direction (CW / CCW)
	this.rotatePerTick = (card.location.getCardRotation(card) - card.rotation) / this.length;

	// Determine the resizing
	this.growPerTick = (card.location.scale - card.scale) / this.length;

	// Determine the offered effect
	if (this.card.offered) {
		this.card.shadow = Card.offeredShadow;
	} else {
		this.card.shadow = null;
	}
	
}

/**
 * This is called by Drawing.tick.
 */
CardAnimation.prototype.tick = function() {
	this.updates++;
	
	if (this.updates >= this.length) { // we are finished
		this.card.x        = this.dest.dx;
		this.card.y        = this.dest.dy;
		this.card.rotation = this.card.location.getCardRotation(this.card);
		this.card.scale = this.card.scaleY = this.card.scaleX = this.card.location.scale;
		
		// Set the zIndex
		if ( ! this.card.offered) {
			this.card.zIndex = this.card.location.getCardZIndex(this.card);
			this.card.floating = false;
			Card.sortCards();
		}
		
		Drawing.removeAnimation(this);
	}
	else if (this.updates > 0){ // normal tick
		if (this.xScaleFactor <= 0) {
			this.xScaleFactor *= -1;
			this.dXScaleFactor *= -1;
			this.card.image = Card.images[this.card.type];
		}
		
		this.card.x += this.unitMove.dx * this.velocity;
		this.card.y += this.unitMove.dy * this.velocity;
		
		this.xScaleFactor += this.dXScaleFactor;
		this.card.scale += this.growPerTick;
		this.card.scaleY = this.card.scale;
		this.card.scaleX = this.card.scale * this.xScaleFactor;
		
		this.card.rotation += this.rotatePerTick;
		
	}	
};

CardAnimation.prototype.doCallback = function() {
	if (this.callback) this.callback(this.card);
}