/**
 * @author: Saxon Jensen
 * @author: Ben Ritter
 */


var GAMENAME = "Mushroom Boom";
var PLAYERID = "";

// DEBUG
var autoJoinName;
var loadIntervalId, loaderOpacity;


var response, loadingIndicator;

var connectedList, notConnectedList, inGameList, notInGameList, playingList, notPlayingList;

function loaderFadeTransition() {
	loaderOpacity -= 0.05;
	document.getElementById("loadStatus").style.opacity = "" + loaderOpacity;
	if (loaderOpacity <= 0) {
		clearInterval(loadIntervalId);
		document.getElementById("loadStatus").style.display = "none";
	}
}

function updateLoader() {
	var perc = Math.round(Preloader.preload.progress * 100);
	document.getElementById("loadStatus").innerHTML = "Loading<br />" + perc + "%";
}

function startLoad() {
	Preloader.setCompleteListener(init);
	Preloader.setProgressListener(updateLoader);
	Preloader.startLoad();
	loaderOpacity = 1;
}

function init() {
	loadIntervalId = window.setInterval(loaderFadeTransition, 30);
	response = document.getElementById("chatBox");
	loadingIndicator = document.getElementById("loadingIndicator");
	
	connectedList    = document.getElementsByClassName("connected");
	notConnectedList = document.getElementsByClassName("notConnected");
	inGameList       = document.getElementsByClassName("inGame");
	notInGameList    = document.getElementsByClassName("notInGame");
	playingList      = document.getElementsByClassName("playing");
	notPlayingList   = document.getElementsByClassName("notPlaying");
	
	hideAll(connectedList);
	hideAll(inGameList);
	hideAll(playingList);
	
	doTheResizeThing();
	
	InfoBubble.el      = document.getElementById("infoBubble");
	InfoBubble.title   = document.getElementById("infoBubbleTitle");
	InfoBubble.content = document.getElementById("infoBubbleContent");
	document.getElementById("infoBubbleClose").addEventListener("click", InfoBubble.hide);
	
	///////////////// Bindings /////////////////////
	document.getElementById("connectButton").addEventListener("click", Connection.openConnection);
	
	document.getElementById("CreateNewGame").addEventListener("click", function() {
		var idi = document.getElementById("newGameId");
		Connection.sendMessage({type: "newGame", id: idi.value});
		idi.value = "";
	});
	
	document.getElementById("chatInput").addEventListener("keydown", chatInputKey);
	
	document.getElementById("lobbyReady").addEventListener("click", function(){
		Connection.sendMessage({type:"ready", ready:true});
	});
	document.getElementById("lobbyNotReady").addEventListener("click", function(){
		Connection.sendMessage({type:"ready", ready:false});
	});
	document.getElementById("lobbyStartGame").addEventListener("click", function(){
		Connection.sendMessage({type:"startGame"});
	});
	
	// FORCE FOCUS ON CHAT
	document.body.addEventListener('mouseup', function() {
		// first we check if we are in game and connected
		//TODO check if there is any text selected
		if (Game.id != null  &&  Connection.conn.readyState == 1)
			document.getElementById("chatInput").focus();
	}, false);
	
	
	// DEBUG
	autoJoinName = getGETByName("autojoin");
	if (autoJoinName) {
		document.getElementById("name").value = autoJoinName;
		document.getElementById("connectButton").click();
	}
}


function doTheResizeThing() {
	var w = document.body.clientWidth - 1050; // canvas size and a bit
	if (w < 200) w = 950;
	
	document.getElementById("chat").style.width = w + "px";
}
window.onresize = doTheResizeThing;


/**
* Asks the server to join a given game.
*/
function joinGame(gameId) {
	Connection.sendMessage(JSON.stringify(
		{
			type: "joinGame",
			id: gameId
		}
	));
}


/**
* Creates a moveCardMessage from the #mc- input elements and send it to the server.
*
* For testing purposes.
*/
function createMoveCardMessage() {
	var message = {type: "moveCard"}
	message.source = document.getElementById("mc-source").value;
	message.sourceIndex = parseInt(document.getElementById("mc-sourceIndex").value);
	message.destination = document.getElementById("mc-destination").value;
	message.destinationIndex = parseInt(document.getElementById("mc-destinationIndex").value);
	Connection.sendMessage(message);
}

/**
* Adds a message to the chat box
*/
function chatBox(message, divClass) {
	response.innerHTML = '<div class="'+divClass+'">' + message + "</div>" + response.innerHTML;
}

function chatInputAppend(text) {
	var e = document.getElementById("chatInput");
	e.value += text;
}
function chatSendMessage(text) {
	Connection.sendMessage({
		type: "chat",
		message: text
	});
}

//ugly global
var isTyping = false;
function chatInputKey(event) {
	if (event.keyCode == 17 || event.keyCode == 16 || event.keyCode == 18) return;
	
	if (Game.id != null  &&  ! isTyping) {
		Connection.sendMessage({type: "typing"});
		isTyping = true;
		setTimeout("isTyping = false", 3000);
	}
	
	if (event.keyCode == 13) {
		chatSendMessage(event.target.value);
		event.target.value = "";
		return false;
	}
	return true;
}


function hideAll(list) {
	for (var i = 0; i < list.length; i++) list[i].style.display = "none";
}
function showAll(list) {
	for (var i = 0; i < list.length; i++) list[i].style.display = "block";
}



function getGETByName(name)
{
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

/**
 * Parses a chat message, expanding [abbv]s.
 */
//TODO move to HTML namespace
function parseChatMessage(message, iconsOnly) {
	for (var k in Card.names) {
		message = message.replace("["+k+"]", (iconsOnly ? HTML.cardIcon(k) : HTML.mushroomName(k)) );
	}
	return message;
}

//TODO move HTML namespace to another file
function HTML() { throw new Error("namespace") }
HTML.cardIcon = function(cardType) {
	return '<img src="Images/card-icon-'+cardType+'.png" alt="'+cardType+'" class="inlineIcon"/>';
}

HTML.mushroomName = function(cardType) {
	return '<span class="mushroomName">' + HTML.cardIcon(cardType) + ' ' + Card.names[cardType] + '</span>';
}

/**
 * Returns html for a button that, when clicked, will send a chat message with the given text.
 *
 * This method does not yet escape the text, so single or double quotes are bad.
 */
HTML.sendAsChatButton = function(text, displayText) {
	displayText = displayText || text;
	return '<div class="sendAsChatButton" onclick="chatSendMessage(\''+text+'\')"><span>'+parseChatMessage(displayText, true)+'</span></div>';
}