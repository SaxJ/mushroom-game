/**
 * A player in the game. This holds the players name and coins and references to all of it's CardLocations.
 *
 * @author Ben Ritter
 */

function Player(id, name) {
	this.id = id;
	this.name = name;
}

Player.prototype.setup = function(index) {
	this.index = index;
	
	this.coins = 0;
	
	// Create CardLocations
	this.fields = new Array(Game.PLAYER_START_FIELDS);
	for (var i = 0; i < Game.PLAYER_START_FIELDS; i++) {
		this.fields[i] = new CardLocation("FIELD", this, i);
	}
	this.hand = new CardLocation("HAND", this);
	this.table = new CardLocation("TABLE", this);
	
	// This is a silly bit of code to "hash" a string to a number between 0 and 20 inclusive.
	var hash = 0;
	for (i = 0; i < this.id.length; i++) hash += this.id.charCodeAt(i);
	hash = hash % 21;
	// finished the silly bit of code

	// Set up this player's face
	this.faceContainer = new Container();
	var faceData = Preloader.getItem("face-" + hash + ".png");
	var face = new Bitmap(faceData);
	var faceBlur = new Bitmap("Images/faces/face-blur.png");
	var speechBubble = new Bitmap("Images/faces/speech-bubble.png");
	this.faceContainer.player = this;
	this.faceContainer.scale = 1;
	faceBlur.alpha = 0;
	speechBubble.alpha = 0;
	speechBubble.x = 37; //TODO Replace this magic number with value from image width
	speechBubble.y = 48; //TODO as above
	this.faceContainer.addChild(faceBlur);
	this.faceContainer.addChild(face);
	this.faceContainer.addChild(speechBubble);
	this.faceContainer.regX = 70 / 2; //TODO replace magic numbers
	this.faceContainer.regY = 80 / 2;
	CardLocation.layout(this.faceContainer, "face");
	Game.container.addChild(this.faceContainer);
	if (this.index == 0) {
		this.faceContainer.onPress = function(evt) {
			Connection.sendMessage({
				type: "tradeRequest",
				trade: false
			});
		}
	} else {
		this.faceContainer.onPress = function(evt) {
			Connection.sendMessage({
				type: "tradeRequest",
				trade: true,
				otherPlayer: evt.target.player.id
			});
		}
	}
		
	this.faceContainer.onMouseOver = function(evt) {
		evt.target.scaleX = evt.target.scaleY = evt.target.scale * 1.05;
		Drawing.update = true;
	};
	
	this.faceContainer.onMouseOut = function(evt) {
		evt.target.scaleX = evt.target.scaleY = evt.target.scale;
		Drawing.update = true;
	};
	
	// Set up the coins and name
	this.nameDisplay = new Text(this.name, "20px bold", "white");
	this.nameDisplay.textAlign = 'center';
	this.nameDisplay.player = this;
	CardLocation.layout(this.nameDisplay, "name");
	Game.container.addChild(this.nameDisplay);
	
	this.coinsDisplay = new Text("0", "20px bold", "white");
        this.coinsDisplay.textAlign = 'center';
	this.coinsDisplay.player = this;
	CardLocation.layout(this.coinsDisplay, "coins");
	Game.container.addChild(this.coinsDisplay);
	
	this.coinStack = CardLocation.createCoinStack(this);
}

Player.prototype.addCoins = function(count) {
	this.coins += count;
	this.coinsDisplay.text = this.coins;
}

Player.prototype.setTrading = function(trading) {
	if (trading) {
		this.faceContainer.scaleX = this.faceContainer.scaleY = this.faceContainer.scale * 1.05;
		var b = this.faceContainer.getChildAt(0);
        b.alpha = 1;
        this.faceContainer.trading = true;
	} else {
		this.faceContainer.scaleX = this.faceContainer.scaleY = this.faceContainer.scale;
        var b = this.faceContainer.getChildAt(0);
        b.alpha = 0;
        this.faceContainer.trading = false;
	}
	Drawing.update = true;
}

Player.prototype.htmlName = function() {
	return '<span class="playerName">'+this.name+'</span>';
}

Player.prototype.isTyping = function() {
	var sb = this.faceContainer.getChildAt(2);
    
	sb.alpha = 1;
	Game.stage.update();
	
	clearTimeout(sb.timeout);
	var thatthis = this;
	sb.timeout = setTimeout(function(){thatthis.isNotTyping()}, 5000);
}

Player.prototype.isNotTyping = function() {
	this.faceContainer.getChildAt(2).alpha = 0;
	Game.stage.update();
}