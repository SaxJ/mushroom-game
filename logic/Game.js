/**
 * The Game class, responsible for storing game state and co-ordinating events
 * from the server.
 * 
 * @author Saxon Jensen
 * @author Ben Ritter
 */

// CONSTANTS
Game.PLAYER_MAX_FIELDS = 3;
Game.PLAYER_START_FIELDS = 2;
Game.EXTRA_FIELD_COST = 3;

Game.HAND_START_SIZE    = 5;
Game.TOTAL_SHUFFLES     = 3;

Game.DRAW_SEEDS_HAND    = 3;
Game.DRAW_SEEDS_TABLE   = 2;

function Game() {
	throw new Error("Static Class: Game");
}

Game.stage = null;
Game.canvas = null;
Game.stock = null;
Game.discard = null;
Game.numPlayers = null;
Game.players = null;
Game.playersById = {};
Game.primaryPlayer = null;

Game.id = null;
Game.state = null; // LOBBY, INGAME, PAUSED, FINISHED

Game.cardsInTheAir = new Array(); // Cards that have been dropped, but the server hasn't told us what to do with them yet

Game.locations = new Array(); // A list of all locations

/**
 * Sets up the internal state of the game and the containers etc.
 */
Game.setup = function(canvasId, playerList, primaryPlayerName) {
	Game.state = "INGAME";
	
	// Set up the playing field and graphics stuff
	Game.canvas = document.getElementById(canvasId);
	Game.stage     = new Stage(Game.canvas);
	Game.container = new Container(); // For all the cards to go in
	Game.stage.addChild(Game.container);
	
	// Set up the players
	Game.numPlayers = playerList.length; // number of players
	// Rotate the player list so that primaryPlayer is in position 0...
	while (playerList[0] != primaryPlayerName) {
		playerList.push(playerList.shift());
	}
	
	Game.players = new Array(Game.numPlayers);
	for (var i = 0; i < Game.numPlayers; i++) {
		Game.players[i] = Game.playersById[playerList[i]];
		Game.players[i].setup(i);
	}
	Game.primaryPlayer = Game.players[0];
	
	
	// Create stock and discard
	Game.stock = new Bitmap(Card.images[0]);
	Game.stock.name = "STOCK";
	Game.container.addChild(Game.stock);
	CardLocation.setStockLocation(Game.stock, Game.numPlayers);
	
	Game.discard = new CardLocation("DISCARD");
	Game.locations.push(Game.discard);
	
	Drawing.drawLocations();
	
	
	///////////////////////////////////////
	Ticker.addListener(Drawing);
	Ticker.setFPS(30);
	if (Touch.isSupported()) {
		Touch.enable(Game.stage);
	}
	
	// enabled mouse over / out events
	Game.stage.enableMouseOver(10);
};


// Message parsing functions
/**
 * Handles a message that came from the server.
 */
Game.interpretMessage = function(message) {
	switch (message.type) {
		case "startGame":
			chatBox("Let's get ready to rumble!", "game");
			Game.setup("gameCanvas", message.order, PLAYERID);
		case "continueGame":
			showAll(   inGameList);
			hideAll(notInGameList);
			showAll(   playingList);
			hideAll(notPlayingList);
			break;
		
		case "gameOver":
			chatBox("That's all folks", "game");
			for (var i in message.players) {
				var p = message.players[i];
				chatBox(p.place + ". " + p.player + " with " + p.coins + " coins", "player");
			}
			break;
		
		case "shuffle":
			chatBox(message.cards + " cards were shuffled (" + message.shufflesToGo + " shuffles to go)", "game");
			Game.discard.cards = [];
			//TODO #20 add animation
			break;
		
		case "moveCard":
			Game.moveCard(message);
			break;
		
		case "harvest":
			Game.harvest(message);
			break;
		
		case "illegalMove":
			chatBox(message.reason, "noCanDo");
			Drawing.animateCard(Game.cardsInTheAir.pop());
			break;
		
		case "offerCard":
			Game.offerCard(message);
			break;
		
		case "tradeRequest":
			Game.tradeRequest(message);
			break;
			
		case "tradeReady":
			Game.tradeReady(message);
			break;
			
		case "doTrade":
			Game.doTrade(message);
			break;
		
		case "turn":
			// Update the turn indicator
			var el   = document.getElementById("turnState");
			var html = Game.playersById[message.player].htmlName() + "'s";
			var pp   = (Game.playersById[message.player] == Game.primaryPlayer);
			if (pp)	html = "your";
			
			if (pp) document.getElementById("turnStateWrapper").style.borderColor = Drawing.TURNCOLOUR;
			else    document.getElementById("turnStateWrapper").style.borderColor = "gray";
			
			html = '<span class="playerName">' + html + '</span>';
			
			switch(message.stage) {
				case "MUST_PLANT":
					if (pp) html = "It is "+html+" turn. Plant your first card."
					else    html = "It is "+html+" turn to plant their first card.";
					break;
				case "CAN_PLANT":
					if (pp) html = "It is "+html+" turn. Plant another card or draw."
					else    html = "It is "+html+" turn. They can plant another card or draw."
					break;
				case "MUST_DRAW":
					if (pp) html = "It is "+html+" turn. Draw from the stock.";
					else    html = "It is "+html+" turn to draw from the stock.";
					break;
				case "TRADING":
					if (pp) html = "It is "+html+" turn and time to trade! Draw cards from the stock when you are finished.";
					else    html = "It is "+html+" turn. Everyone may now trade with them.";
					break;
				case "WAITING":
					html = html + " turn is over, but everybody must plant their traded cards.";
					break;
			}
			
			el.innerHTML = html;
			break;
		
		default:
			console.log(message);
			throw new Error("Unknown message type: " + message.type);
	}
}

/**
 * 
 */


/**
 * Figures out where to move a card and moves it there, based on a message from the server.
 * 
 * @param message a moveCard message from the server
 */
Game.moveCard = function(message) {
	var card, dest;

	if (message.source == "STOCK") {
		card = Card.createCard();
		Game.container.addChild(card);
		//TODO check if the stock is empty to change the look of the stock
	} else if (message.source == "HAND") {
		card = Game.getLocation("HAND", message.sourcePlayer).cards[message.sourceIndex];
	} else if (message.source == "TABLE") {
		card = Game.getLocation("TABLE", message.sourcePlayer).cards[message.sourceIndex];
	} else {
		throw new Error("Uknown source: " + message.source);
	}
	
	card.offered = false;

	dest = Game.getLocation(message.destination, message.destinationPlayer, message.destinationIndex);
	
	card.type = message.cardType;
	Game.moveCardTo(card, dest);
}

/**
 * Harvests a field from a server message. That is, some cards are turned into coins, some are moved to the discard.
 */
Game.harvest = function(message) {
	var field = Game.playersById[message.player].fields[message.field];
	for (var i = field.cards.length-1, c = 0; i >= 0; i--, c++) {
		if (c < message.coins) {
			var card = field.cards[i];
			card.type = -1;
			Game.moveCardTo(card, Game.playersById[message.player].coinStack);
		}
		else {
			Game.moveCardTo(field.cards[i], Game.discard);
		}
	}
}


/**
 * Marks a card as offered or not.
 */
Game.offerCard = function(message) {
	var card = Game.getLocation(message.location, message.player).cards[message.index];
	card.offered = message.offer;
	card.type = message.cardType;
	Drawing.animateCard(card);
}

/**
 * Handles a trade request.
 */
Game.tradeRequest = function(message) {
	var p = Game.playersById[message.player];
	var o = Game.playersById[message.otherPlayer];
	
	if (Game.primaryPlayer == p) { // We're trading with someone else
		p.setTrading(message.trade);
		o.setTrading(message.trade);
		if (message.trade) chatBox("You offered to trade with " + o.htmlName(), "game");
		else chatBox("You are no longer offering to trade with " + o.htmlName(), "game");
	}
	else { // Someone trading with us
		p.setTrading(message.trade);
		if (message.trade) chatBox(p.htmlName() + " offered to trade with you", "game");
		else chatBox(p.htmlName() + " is no longer offering to trade with you", "game");
	}
}

/**
 * Handles a doTrade.
 */
Game.doTrade = function(message) {
	var p = Game.playersById[message.player];
	var o = Game.playersById[message.otherPlayer];
	if (o == Game.primaryPlayer) {
		var temp = p;
		p = o;
		o = temp;
	}
	
	if (p == Game.primaryPlayer) {
		chatBox("You traded with " + o.htmlName(), "game");
	} else {
		chatBox(p.htmlName() + " and " + o.htmlName() + " traded", "game");
	}
	
	p.setTrading(false);
	o.setTrading(false);
}

// Doing stuff functions
/**
 * Moves a card to a destination, updating all of it's internals.
 *
 * @param card the card to move
 * @param dest the destination cardLocation for the card
 */
 Game.moveCardTo = function(card, dest) {
	if (card.location != null) {
		card.location.removeCard(card);
	}
	card.location = dest;
	card.locationIndex = dest.cards.length;
	dest.addCard(card);
	
	var callback = null;
	if (dest.name == "coinStack") callback = Card.turnIntoCoin;
	//if (dest.name == "DISCARD")   callback = Game.emptyDiscard;
	
	Drawing.animateCard(card, {callback: callback});
};

/**
 * Sends a moveCard message to the server.
 */
Game.sendMoveCard = function(card, destination) {
	if (card.location == destination) return Drawing.animateCard(card);
	
	Game.cardsInTheAir.push(card);
	
	var msg = {
		type: "moveCard",
		source: card.location.name,
		sourceIndex: card.locationIndex,
		destination: destination.name
	};
	if (destination.name == "FIELD")   msg.destinationIndex = destination.fieldIndex;
	if (card.location.name == "FIELD") msg.sourceIndex = card.location.fieldIndex;
	Connection.sendMessage(msg);
};

/**
 * Gets the CardLocation that represents a card location.
 */
Game.getLocation = function(locationName, playerId, fieldIndex) {
	switch (locationName) {
		case "HAND":
			return Game.playersById[playerId].hand;
		case "TABLE":
			return Game.playersById[playerId].table;
		case "FIELD":
			return Game.playersById[playerId].fields[fieldIndex];
		default:
			throw new Error("Uknown cardLocation: " + locationName);
	}
};

/**
 * Destroys all cards in the discard that are not the given card.
 */
Game.emptyDiscard = function(card) {
	for (var i = Game.discard.cards.length - 1; i >= 0; i--) {
		if (Game.discard.cards[i] != card) {
			Card.destroy(Game.discard.cards[i]);
		}
	}
}

function leaveGame() {
	Game.id = null;
	Game.state = null;
	hideAll(inGameList);
	showAll(notInGameList);
	hideAll(playingList);
	showAll(notPlayingList);
}