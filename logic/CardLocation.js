/**
 * A "location" that cards can be in (such as "HAND" or "FIELD" etc.).
 * 
 * @author Ben Ritter
 */

CardLocation.INITIAL_SCALE = 0.8;

CardLocation.SCALED_CARD_WIDTH  = Card.WIDTH  * CardLocation.INITIAL_SCALE;
CardLocation.SCALED_CARD_HEIGHT = Card.HEIGHT * CardLocation.INITIAL_SCALE;

/**
 * Creates a new CardLocation and sets it's location and stuff.
 */
function CardLocation(name, player, fieldIndex) {
	this.cards = new Array();
	this.name = name;
	this.player = player;
	
	// Defaults
	this.droppable = false;
	this.scale = CardLocation.INITIAL_SCALE;
	this.rotation = 0;
	
	// Card layout inside the container
	this.firstCardXOff = Card.WIDTH  / 2;
	this.firstCardYOff = Card.HEIGHT / 2;
	this.otherCardXOff = Card.WIDTH;
	this.otherCardYOff = 0;
	
	this.baseZIndex       = 0;
	this.zIndexMultiplier = 1;
	
	CardLocation.layout(this, name, fieldIndex);
	Game.locations.push(this);
}

/**
 * Creates a CoinStack which is a pseudo CardLocation.
 */
CardLocation.createCoinStack = function(player) {
	var ret = new Bitmap("Images/coins.png");
	
	ret.cards = new Array();
	ret.name = "coinStack";
	ret.player = player;
	
	ret.rotation = 0;
	ret.scale = CardLocation.INITIAL_SCALE;
	
	ret.firstCardXOff = 20;
	ret.firstCardYOff = 20;
	ret.otherCardXOff = 0;
	ret.otherCardYOff = 0;
	
	ret.baseZIndex       = 90;
	ret.zIndexMultiplier = 0;
	
	// Gotta do some binding
	ret.getCardPos      = CardLocation.prototype.getCardPos;
	ret.getCardRotation = CardLocation.prototype.getCardRotation;
	ret.getCardZIndex   = CardLocation.prototype.getCardZIndex;
	ret.addCard = function(card) { this.cards.push(card) }
	ret.removeCard = function(card) {
		for (var i = 0; i < this.cards.length; i++) {
			if (card == this.cards[i]) {
				this.cards.splice(i, 1);
				return;
			}
		}
	}
	
	CardLocation.layout(ret, "coinStack");
	Game.container.addChild(ret);
	
	return ret;
}

/**
 * Sets the location etc. for a thing based on it's name.
 */
CardLocation.layout = function(thing, name, fieldIndex) {
	var padding = 10;
	
	// Sizes are relative to the player where applicable
	switch (name) {
		case "HAND":
			thing.x                = 0;
			thing.y                = 2 * CardLocation.SCALED_CARD_HEIGHT;
			thing.width            = 8 * Card.WIDTH;
			thing.height           = Card.HEIGHT;
			thing.firstCardXOff   += 7 * Card.WIDTH;
			thing.otherCardXOff    = -Card.WIDTH;
			thing.baseZIndex       = 30;
			thing.zIndexMultiplier = -1;
			break;
			
		case "TABLE":
			thing.x          = 2 * CardLocation.SCALED_CARD_WIDTH + 2 * padding;
			thing.y          = 0;
			thing.width      = 4 * Card.WIDTH;
			thing.height     = Card.HEIGHT;
			thing.baseZIndex = 60;
			break;
			
		case "FIELD":
			thing.fieldIndex = fieldIndex;
			
			thing.x             = thing.fieldIndex * CardLocation.SCALED_CARD_WIDTH + thing.fieldIndex * padding;
			thing.y             = 0;
			thing.width         = Card.WIDTH;
			thing.height        = 1.9 * Card.HEIGHT;
			thing.otherCardXOff = 0;
			thing.otherCardYOff = padding;
			thing.baseZIndex    = 31;
			
			if (thing.player.index == 0) thing.droppable = true;
			break;
		
		case "face":
			thing.x = 6 * CardLocation.SCALED_CARD_WIDTH + 6 * padding + 5;
			thing.y = 40;
			break;
			
		case "name":
			thing.x = 6 * CardLocation.SCALED_CARD_WIDTH + 6 * padding + 5;
			thing.y = 90;
			break;
		
		case "coinStack":
			thing.x = 6 * CardLocation.SCALED_CARD_WIDTH + 4 * padding + 50;
			thing.y = 0;
			break;
		
		case "coins":
			thing.x = 6 * CardLocation.SCALED_CARD_WIDTH + 4 * padding + 70;
			thing.y = 50;
			break;
			
		case "DISCARD":
			thing.x           = (Game.canvas.width / 2) - (CardLocation.SCALED_CARD_WIDTH) - 10;
			thing.y           = (Game.canvas.height / 2) - CardLocation.SCALED_CARD_HEIGHT / 2;
			thing.width       = Card.WIDTH;
			thing.height      = Card.HEIGHT;
			thing.baseZIndex    = -90;
			
			thing.otherCardXOff = 0;
			thing.oldGetCardPos = thing.getCardPos;
			thing.getCardPos = function(card) {
				var v = this.oldGetCardPos(card);
				
				if (card.locationIndex == 0) return v;
				
				v.dx += (Math.sin(card.id) * 7);
				v.dy += (Math.sin(1.2 * card.id) * 7)
				return v;
			}
			thing.getCardRotation = function(card) {
				var m = (Math.sin(1.378 * card.id) * 10);
				if      (card.locationIndex == 0) m = 0;
				else if (card.locationIndex > 40) m *= 2;
				return this.rotation + m;
			}
			break;
		
		default:
			throw new Error("What the hell, tried creating a container with name " + name);
	}
	
	
	if (thing.player) CardLocation.perPlayerLayout(thing);
}

/**
 * Sets the location of the stock.
 */
CardLocation.setStockLocation = function(stock) {
	stock.regX = Card.WIDTH / 2;
	stock.regY = Card.HEIGHT / 2;
	
	stock.x = (Game.canvas.width / 2) + (CardLocation.SCALED_CARD_WIDTH / 2) + 10;
	stock.y = (Game.canvas.height / 2);
	
	stock.scaleX = stock.scaleY = stock.scale = CardLocation.INITIAL_SCALE;
	
	stock.onPress = function(evt) {
		Connection.sendMessage({type: "moveCard", source: "STOCK"});
	}
};

/**
 * Rotates, scales and moves something so that it is positioned globally in the right for a given player.
 */
CardLocation.perPlayerLayout = function(thing){
	
	var pHeight = 3 * CardLocation.SCALED_CARD_HEIGHT;
	var pWidth  = 8 * CardLocation.SCALED_CARD_WIDTH;
	
	var ops = 0.8; // "other player scale"
	
	switch (thing.player.index) {
		case 0:
			//CardLocation.rotate(thing, 0);
			//CardLocation.scale(thing, 1);
			thing.x += Game.canvas.width / 2 - pWidth / 2;
			thing.y += Game.canvas.height - pHeight;
			break;
			
		case 1:
			CardLocation.rotate(thing, 90);
			CardLocation.scale(thing, ops);
			thing.x += ops * pHeight;
			thing.y += Game.canvas.height - ops * pWidth;
			break;
			
		case 2:
			CardLocation.rotate(thing, 180);
			CardLocation.scale(thing, ops);
			thing.x += Game.canvas.width / 2;
			thing.y += ops * pHeight;
			break;
			
		case 3:
			CardLocation.rotate(thing, 180);
			CardLocation.scale(thing, ops);
			thing.x += Game.canvas.width / 2 + pWidth;
			thing.y += ops * pHeight;
			break;
			
		case 4:
			CardLocation.rotate(thing, 270);
			CardLocation.scale(thing, ops);
			thing.x += Game.canvas.width - ops * pHeight;
			thing.y += Game.canvas.height;
			break;
		
		default: new Error("how did we get here?");
	}
}

/**
 * Rotates the cardLocation around the origin by a number of degrees and updates it's rotation.
 */
CardLocation.rotate = function(thing, degrees) {
	thing.rotation += degrees;
	
	var v = new Vector(thing.x, thing.y);
	v.rotate(degrees);
	thing.x = v.dx;
	thing.y = v.dy;
}

/**
 * Scales the cardLocation's position and scale by a factor.
 */
CardLocation.scale = function(thing, factor) {
	thing.x     *= factor;
	thing.y     *= factor;
	thing.scale *= factor;
	thing.scaleX *= factor;
	thing.scaleY *= factor;
}

// Calculated what the new otherCardXOff should be and if needed, animates all cards to their new spot
CardLocation.prototype.updateCardPositions = function() {
	if (this.cards.length == 0) return false;

	if (this.initialXOff === undefined)
		this.initialXOff = Math.max(this.otherCardXOff, -this.otherCardXOff);
	
	var neg = false;
	if (this.otherCardXOff < 0) neg = true;
	
	var diff;
	if (neg) diff = this.firstCardXOff;
	else diff = this.width - this.firstCardXOff;
	
	var newVal = Math.min(diff / this.cards.length, this.initialXOff);
	if (neg) newVal = -newVal;
	if (newVal != this.otherCardXOff) {
		this.otherCardXOff = newVal;
		
		for (var i = 0; i < this.cards.length; i++)
			Drawing.animateCard(this.cards[i], {delay: 0});
		
		return true;
	}
	
	return false;
}

CardLocation.prototype.addCard = function(card) {
	this.cards.push(card);
	this.updateCardPositions();
}

CardLocation.prototype.removeCard = function(card) {
	this.cards.splice(card.locationIndex, 1);
	
	for (var i = card.locationIndex; i < this.cards.length; i++)
		this.cards[i].locationIndex = i;
		
	if ( ! this.updateCardPositions())
		for (var i = card.locationIndex; i < this.cards.length; i++)
			Drawing.animateCard(this.cards[i], {delay: 0});
}

/**
 * Get's the position that a card in this cardLocation should be.
 * 
 * @param index the index of the card
 * @return a vector of the position
 */
CardLocation.prototype.getCardPos = function(card) {
	if (card.location != this) throw new Error("Called location.getCardPos with a card that is not in this location");
	
	var xOff = this.firstCardXOff + this.otherCardXOff * card.locationIndex;
	var yOff = this.firstCardYOff + this.otherCardYOff * card.locationIndex;
	if (card.offered) yOff -= 15;
	var v = new Vector(xOff, yOff);
	v.rotate(this.rotation);
	v.dx *= this.scale;
	v.dy *= this.scale;
	v.dx += this.x;
	v.dy += this.y;
	return v;
}

/**
 * The default getCardRotation. The rotation that the card should be at is returned.
 */
CardLocation.prototype.getCardRotation = function(card) {
	return this.rotation;
}

/**
 * Calculates the zIndex of the card.
 */
CardLocation.prototype.getCardZIndex = function(card) {
	return this.baseZIndex + this.zIndexMultiplier * card.locationIndex;
}

/**
 * @return true iff (x, y) is in this cardLocation.
 */
CardLocation.prototype.hitTest = function(x, y) {
	// (relX, relY) is relative to the top-left corner
	var relX = x - this.x;
	var relY = y - this.y;

	var point  = new Vector(relX, relY);
	point.rotate(-this.rotation);
	return 0 <= point.dx && point.dx <= this.width  &&  0 <= point.dy && point.dy <= this.height;
};
