/**
 *Contains javascript used to begin loading of resources.
 */

var Preloader = {
    preload: new createjs.PreloadJS(true),
    percentage: 0,
    complete: false,
    
    manifest: [
	{src: './Images/card-0.png', id: 'card-0.png'},
	{src: './Images/card_demo.png', id: 'card_demo.png'},
	{src: './Images/card-icon-10.png', id: 'card-icon-10.png'},
	{src: './Images/card-20.png', id: 'card-20.png'},
	{src: './Images/card-10.png', id: 'card-10.png'},
	{src: './Images/template.png', id: 'template.png'},
	{src: './Images/card-icon-16.png', id: 'card-icon-16.png'},
	{src: './Images/card-icon-12.png', id: 'card-icon-12.png'},
	{src: './Images/card-6.png', id: 'card-6.png'},
	{src: './Images/card-8.png', id: 'card-8.png'},
	{src: './Images/card-16.png', id: 'card-16.png'},
	{src: './Images/card-icon-6.png', id: 'card-icon-6.png'},
	{src: './Images/loading.png', id: 'loading.png'},
	{src: './Images/loading.gif', id: 'loading.gif'},
	{src: './Images/card-icon-18.png', id: 'card-icon-18.png'},
	{src: './Images/coins.png', id: 'coins.png'},
	{src: './Images/card-14.png', id: 'card-14.png'},
	{src: './Images/coin.png', id: 'coin.png'},
	{src: './Images/card-12.png', id: 'card-12.png'},
	{src: './Images/tut/overview.png', id: 'overview.png'},
	{src: './Images/faces/face-13.png', id: 'face-13.png'},
	{src: './Images/faces/face-15.png', id: 'face-15.png'},
	{src: './Images/faces/face-6.png', id: 'face-6.png'},
	{src: './Images/faces/face-11.png', id: 'face-11.png'},
	{src: './Images/faces/face-blur.png', id: 'face-blur.png'},
	{src: './Images/faces/face-12.png', id: 'face-12.png'},
	{src: './Images/faces/face-1.png', id: 'face-1.png'},
	{src: './Images/faces/face-2.png', id: 'face-2.png'},
	{src: './Images/faces/face-16.png', id: 'face-16.png'},
	{src: './Images/faces/face-5.png', id: 'face-5.png'},
	{src: './Images/faces/face-0.png', id: 'face-0.png'},
	{src: './Images/faces/face-18.png', id: 'face-18.png'},
	{src: './Images/faces/face-8.png', id: 'face-8.png'},
	{src: './Images/faces/face-20.png', id: 'face-20.png'},
	{src: './Images/faces/face-4.png', id: 'face-4.png'},
	{src: './Images/faces/face-17.png', id: 'face-17.png'},
	{src: './Images/faces/face-7.png', id: 'face-7.png'},
	{src: './Images/faces/face-3.png', id: 'face-3.png'},
	{src: './Images/faces/face-10.png', id: 'face-10.png'},
	{src: './Images/faces/face-9.png', id: 'face-9.png'},
	{src: './Images/faces/speech-bubble.png', id: 'speech-bubble.png'},
	{src: './Images/faces/face-14.png', id: 'face-14.png'},
	{src: './Images/faces/face-19.png', id: 'face-19.png'},
	{src: './Images/card-icon-20.png', id: 'card-icon-20.png'},
	{src: './Images/card-18.png', id: 'card-18.png'},
	{src: './Images/card-icon-8.png', id: 'card-icon-8.png'},
	{src: './Images/card-icon-14.png', id: 'card-icon-14.png'},
	{src: './Images/speech-corner.png', id: 'speech-corner.png'}
    ],
    
    startLoad: function() {
        Preloader.preload.loadManifest(Preloader.manifest, true);
    },
    
    onProgress: function(e) {
        Preloader.progress = Preloader.preload.progress;
    },
    
    getItem: function(id) {
        console.log("Getting id: " + id);
        var data = Preloader.preload.getResult(id);
        console.log(data);
        return data.result;
    },
    
    setProgressListener: function(funct) {
        Preloader.preload.onProgress = funct;
    },
    
    setCompleteListener: function(funct) {
        Preloader.preload.onComplete = funct;
    }
}