function Actions() {
	throw new Error("Action: Static Class");
}


Actions.handleDropCard = function(card) {
	for (i in Game.locations) {
		var loc = Game.locations[i];
		if (loc.droppable && loc.hitTest(card.x, card.y)) {
			Game.sendMoveCard(card, loc);
			return;
		}
	}
	
	Drawing.animateCard(card);
};
