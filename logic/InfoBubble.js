/**
 * A 'bubble' that sits next to a DisplayObject giving info about it.
 *
 * @author Ben Ritter
 */

function InfoBubble() {
	throw new Error("static class");
}

InfoBubble.dc = null;

InfoBubble.showForCard = function(card) {
	var title = HTML.mushroomName(card.type);
	
	var content = '<div class="appendToChatButton" onclick="chatInputAppend(\' ['+card.type+'] \')"><span>' + HTML.cardIcon(card.type) + '</span></div>';
	
	if (card.location.name == "FIELD") {
		title += " field";
		
		content = "<div>"+card.location.cards.length+" card(s) worth "+Card.coinValue(card.type, card.location.cards.length)+"</div>" + content;
	}
	
	else { // not in a field
		if (card.location.player == Game.primaryPlayer) {
			content += HTML.sendAsChatButton("I want ["+card.type+"]s");
		} else {
			content += HTML.sendAsChatButton(card.location.player.name + ", I want your ["+card.type+"]",
			                                 "I want your ["+card.type+"]")
		}
	}
	
	//TODO expand on this content
	
	InfoBubble.dc = card.getDoubleClick();
	if (InfoBubble.dc != null) content += '<div class="dcButton" onclick="InfoBubble.doDC()">' + InfoBubble.dc.name + '</div>';
	
	InfoBubble.set(card, title, content);
}
InfoBubble.set = function(o, title, content) {
	var p = o.localToGlobal(-o.regX, -o.regY);
	InfoBubble.el.style.top  = (p.y + 50)  + "px";
	InfoBubble.el.style.left = (p.x + 100) + "px";
	
	InfoBubble.title.innerHTML   = title;
	InfoBubble.content.innerHTML = content;
	
	InfoBubble.el.style.display = "block";
}

InfoBubble.appendContent = function(html) {
	InfoBubble.content.innerHTML = InfoBubble.content.innerHTML + html;
}

/**
 * Removes all infobubbles.
 */
InfoBubble.hide = function() {
	InfoBubble.el.style.display = "none";
}

/**
 * Does the double click function for a card.
 */
InfoBubble.doDC = function() {
	Connection.sendMessage(InfoBubble.dc.message);
	InfoBubble.hide();
}