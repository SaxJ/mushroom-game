function DummyGame(){}

DummyGame.interpretMessage = function (message) {
	switch (message.type) {
		case "startGame":
			chatBox("Game Started!", "game");
			break;
		
		case "moveCard":
			chatBox(message.destinationPlayer + " now has a " + message.cardType + " in their " + message.destination + " from " + message.source, "game");
			break;
		
		default:
			chatBox("Don't know what to do with message type " + message.type, "error");
	}
}
